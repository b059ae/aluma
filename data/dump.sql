-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Июл 18 2017 г., 10:11
-- Версия сервера: 5.6.33-79.0
-- Версия PHP: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u0193363_aluma`
--

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_admins`
--

DROP TABLE IF EXISTS `easyii_admins`;
CREATE TABLE `easyii_admins` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `auth_key` varchar(128) DEFAULT NULL,
  `access_token` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_article_categories`
--

DROP TABLE IF EXISTS `easyii_article_categories`;
CREATE TABLE `easyii_article_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_article_categories`
--

INSERT INTO `easyii_article_categories` (`id`, `title`, `description`, `image_file`, `order_num`, `slug`, `tree`, `lft`, `rgt`, `depth`, `status`) VALUES
  (1, 'Новости', '', NULL, 1, 'news', 1, 1, 2, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_article_items`
--

DROP TABLE IF EXISTS `easyii_article_items`;
CREATE TABLE `easyii_article_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_article_items`
--

INSERT INTO `easyii_article_items` (`id`, `category_id`, `title`, `image_file`, `short`, `text`, `slug`, `time`, `views`, `status`) VALUES
  (1, 1, 'Чемпионат России по рыбной ловле', 'article/040200-3029352d50.jpg', 'Всего приняло участие 19 региональных команд из России, это 33 команды (132 спортсмена).', '<p>С 15 по 19 марта 2017 года на Нижегородской Ярмарке проходила выставка «Охота. Рыбалка. Туризм. Отдых», в которой принимала участие компания Торгово-Сервисный Центр Техномакс. На этой выставке она представила моторную лодку «Волжанка», а именно лодку «51 Двухконсольная». Всего выставку посетило более 15 000 человек, среди которых оказалось большое число людей непонаслышке знакомых с «Волжанкой» или желающих познакомиться с лодками.</p><p><br><br>Люди проявляли живой интерес к этой лодке, отмечали, что много слышали об этом бренде, сравнивали с лодками других производителей и находили гораздо больше плюсов именно у Самарского производителя. Особенно Волжанка понравилась тем, кто профессионально занимается рыбалкой – люди хвалили качество производства, приемлемую цену и возможность индивидуального подхода при сборке каждой лодки.</p>', 'cempionat-rossii-po-rybnoj-lovle', 1494314534, 0, 0),
  (2, 1, 'Выставка в Стокгольме', NULL, '18 Марта состоится выставка в Стокгольме, на которой будет представлена новая лодка Aluma Fish 4.7', '<p>В три часа ночи нас ни кто не ждал, поэтому решили \r\nприснуть до рассвета в машине. Разбудил нас ветер,порывами пытающийся \r\nперевернуть наш боевой Тритон. Обмерзшие стекла и -3°С на бортовом \r\nкомпьютере сказали – «в шортах и сланцах долго не протянете». Быстренько\r\n сменив их на три слоя разной одежды, наконец решились выйти на берег. \r\nПричал был полон, т.е. все шесть мест небольшого пирсика, под натиском \r\nветродуя, пытались раздолбать носами и бортами катера \r\nдрузей-конкурентов. Петин Желток, Вираж Димы и Сережи, Северностоличный \r\nСильвер, Живописный крейсер Сереги Жмура и Кировский басбот - все \r\nэкипажи из первой десятки. Очень неудобный старт из края зоны (от \r\nгидроузла в Балаково) выгнал эту звездную команду за сорок километров в \r\nсторону Саратова. </p><p>Ну что ж, по крайней мере, в месте первых \r\nтренировочных дней мы не ошиблись. Адаптировавшись к погоде, выходим на \r\nводу и, взяв за основу опыт прошлых лет, летим меж островами в \r\nСовхозную(Немецкую) воложку, тут же вылетаем на меляк. </p><p><img src="http://aluma.local/uploads/redactor/592fc0fb091d0.jpg" "=""></p><p>Слава богу, что наша Волжаночка даже на 15-20 см \r\nводы может продолжать двигаться. Опыт опытом, а карта глубин Кости \r\nКудинова оказалась очень кстати. Со второго захода уходим в «Щель» и, \r\nнаконец, вот она «Совхозная». Красивейший прибрежный и глубинный \r\nкоряжник открылся нашим глазам при помощи 12-го Лоренса (как нам его не \r\nхватало в прошлом Балаковском этапе PAL!).</p><p>Тактику выбрали такую – делаем проход вдоль берега,\r\n отмечая видимые в боковых лучах коряжки у берега, следующим галсом \r\nпрописываем уже более глубокие предполагаемые «засады хищника». Очень \r\nкрасиво, особенно ближе к выходу из протоки. Несколько постановок – \r\nЭльдорадо нет. <br>Одна щучка до полутора и две разодранные в хлам приманки, причем одна - явно крупным судачком. <br></p>', 'vystavka-v-stokgolme', 1496301602, 0, 0),
  (3, 1, 'Впечатления от Aluma Fish', 'article/108200-7b7916dd2d.jpg', 'Экипаж Сергея Старикова и Сергея Лебедева обкатал новую лодку.', '<p>С 15 по 17 сентября 2016 года состоится ежегодный спиннинговый водно-моторный командный турнир "Джиг-Пари".</p><p><br><br>Традиционно местом проведения станет рыболовная база «Разнежье» на Чебоксарском водохранилище. </p><p><br><br>В 2015 году в соревнованиях приняли участие более 120 команд.</p><p><br><br>Начиная с 2016 года к регистрации и участию допускаются команды только по системе "два участника + видеорегистраторы".</p><p><br><br> </p><p><br><br>О правилах турнирах, а также положении, призах и условиях регистрации читайте на официальном сайте турнира.</p><p><br><br>В рамках соревнований пройдут тест-драйвы катеров и семинары от известных рыболовов.</p><p><br><br>За ВТОРОЕ КОМАНДНОЕ место участники получат Катер ВОЛЖАНКА 46 fish от генерального спонсора ДЖИГ-ПАРИ 2016 компании АБСОЛЮТ-МАРИН.</p><p><br><br>P.S. За такой приз стоит постараться! ;-)</p>', 'vpecatlenia-ot-aluma-fish', 1496301852, 0, 0),
  (4, 1, 'Спуск на воду Fish 4.7', 'article/6200-97008ea270.jpg', 'Состоялся тестовый спуск на воду новой модели Aluma Fish 4.7', '<p>С 10 по 19 февраля приглашаем вас на наш стенд на выставке Vene 17 Båt в городе Хельсинки, Финляндия.<br></p><p><br></p><h3>Messukeskus Car Park and Conference Car Park</h3><p>The Messukeskus Car Park is accessible to all and always open. It is \r\nlocated on the same side as the Northern Entrance. The address for GPS \r\ndevices is Ratapihantie 17.<br>\r\nThe parking fee is €12, which allows you to park once and stay for 24 hours.</p><ul><li>The parking fee is €12, which allows you to park once and stay for 24 hours.</li><li>Parking fees can be paid for either at automated fee collection machines or inside the centre at ticket sales.</li><li>You can pay in cash or with the most common payment cards.</li><li>You can pay when you arrive.</li><li>The car park has 20 disabled parking spaces. Parking is free for people who have a disabled parking permit.</li></ul>', 'spusk-na-vodu-fish-47', 1496301933, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_carousel`
--

DROP TABLE IF EXISTS `easyii_carousel`;
CREATE TABLE `easyii_carousel` (
  `id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_categories`
--

DROP TABLE IF EXISTS `easyii_catalog_categories`;
CREATE TABLE `easyii_catalog_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_categories`
--

INSERT INTO `easyii_catalog_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
  (1, 'Лодки', '', NULL, '[{"name":"video","title":"ID \\u0432\\u0438\\u0434\\u0435\\u043e \\u043d\\u0430 Youtube","type":"string","options":""},{"name":"video2","title":"ID \\u0432\\u0438\\u0434\\u0435\\u043e \\u043d\\u0430 Youtube","type":"string","options":""},{"name":"characteristics","title":"\\u0425\\u0430\\u0440\\u0430\\u043a\\u0442\\u0435\\u0440\\u0438\\u0441\\u0442\\u0438\\u043a\\u0438","type":"html","options":""},{"name":"include","title":"\\u0412\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u043e \\u0432 \\u0441\\u0442\\u043e\\u0438\\u043c\\u043e\\u0441\\u0442\\u044c","type":"html","options":""},{"name":"width","title":"\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430","type":"string","options":""},{"name":"length","title":"\\u0414\\u043b\\u0438\\u043d\\u0430","type":"string","options":""},{"name":"people","title":"\\u0412\\u043c\\u0435\\u0441\\u0442\\u0438\\u043c\\u043e\\u0441\\u0442\\u044c","type":"string","options":""},{"name":"engine","title":"\\u041c\\u0430\\u043a\\u0441. \\u043c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c \\u0434\\u0432\\u0438\\u0433\\u0430\\u0442\\u0435\\u043b\\u044f","type":"string","options":""},{"name":"tank","title":"\\u041e\\u0431\\u044a\\u0435\\u043c \\u0442\\u043e\\u043f\\u043b\\u0438\\u0432\\u043d\\u043e\\u0433\\u043e \\u0431\\u0430\\u043a\\u0430","type":"string","options":""},{"name":"weight","title":"\\u041c\\u0430\\u0441\\u0441\\u0430","type":"string","options":""}]', 'lodki', 1, 1, 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_items`
--

DROP TABLE IF EXISTS `easyii_catalog_items`;
CREATE TABLE `easyii_catalog_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `available` int(11) DEFAULT '1',
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `data` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_items`
--

INSERT INTO `easyii_catalog_items` (`id`, `category_id`, `title`, `description`, `available`, `price`, `discount`, `data`, `image_file`, `slug`, `time`, `status`) VALUES
  (1, 1, 'Fish 4.7', '<h2 "="">КАТЕР для рыбалки И ОТДЫХА<br></h2><p "="" style="text-align: justify;">Катер <strong>ALUMA</strong><strong> </strong><strong>FISH</strong><strong> – </strong>это катер европейского качества по цене, доступной для российского потребителя.  Серия катеров ALUMA отличается высокой надежностью и создана для эксплуатации в любых, даже самых суровых, условиях. Основная концепция производства – <strong><i>изготовление легкого, сверхпрочного, непотопляемого  корпуса.</i></strong><i><strong> </strong></i></p><p "="" style="text-align: justify;"><em><i><strong></strong>Мы сделали катера </i> <strong>«</strong><strong>ALUMA</strong><strong>»</strong>   сверх <strong>комфортными и функциональными</strong>. </em>В моделях катеров <strong>«</strong><strong>ALUMA</strong><strong>» </strong><strong>проработана каждая деталь эстетического дизайна, </strong>которая воплощена в идеальном качестве сборки.</p>', 1, 480000, NULL, '{"video":"oCORlhdkPTs","video2":"VQLeynaTFdI","characteristics":"<table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<th>\\u0422\\u0438\\u043f \\u0438\\u0437\\u0433\\u043e\\u0442\\u043e\\u0432\\u043b\\u0435\\u043d\\u0438\\u044f \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>\\u0446\\u0435\\u043b\\u044c\\u043d\\u043e\\u0441\\u0432\\u0430\\u0440\\u043d\\u043e\\u0439\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0422\\u0438\\u043f \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>\\u043e\\u0442\\u043a\\u0440\\u044b\\u0442\\u044b\\u0439<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0414\\u043b\\u0438\\u043d\\u0430 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u043d\\u0430\\u0438\\u0431\\u043e\\u043b\\u044c\\u0448\\u0430\\u044f\\r\\n\\t<\\/th>\\r\\n\\t<td>5,1 \\u043c <\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0413\\u0430\\u0431\\u0430\\u0440\\u0438\\u0442\\u043d\\u0430\\u044f \\u0434\\u043b\\u0438\\u043d\\u0430 \\u0441\\u0443\\u0434\\u043d\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>4,76 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u043d\\u0430\\u0438\\u0431\\u043e\\u043b\\u044c\\u0448\\u0430\\u044f\\r\\n\\t<\\/th>\\r\\n\\t<td>2,27 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0413\\u0430\\u0431\\u0430\\u0440\\u0438\\u0442\\u043d\\u0430\\u044f \\u0448\\u0438\\u0440\\u0438\\u043d\\u0430 \\u0441\\u0443\\u0434\\u043d\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>2,27 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0428\\u0438\\u0440\\u0438\\u043d\\u0430 \\u043f\\u043e \\u0441\\u043a\\u0443\\u043b\\u0435\\r\\n\\t<\\/th>\\r\\n\\t<td>1,70 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430 \\u0431\\u043e\\u0440\\u0442\\u0430 \\u043d\\u0430 \\u043c\\u0438\\u0434\\u0435\\u043b\\u0435\\r\\n\\t<\\/th>\\r\\n\\t<td>0,99 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430 \\u043d\\u0430\\u0434\\u0432\\u043e\\u0434\\u043d\\u043e\\u0433\\u043e \\u0431\\u043e\\u0440\\u0442\\u0430 \\u043d\\u0430 \\u043c\\u0438\\u0434\\u0435\\u043b\\u0435\\r\\n\\t<\\/th>\\r\\n\\t<td>0,76 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430 \\u0431\\u043e\\u0440\\u0442\\u0430 \\u0432 \\u043a\\u043e\\u043a\\u043f\\u0438\\u0442\\u0435\\r\\n\\t<\\/th>\\r\\n\\t<td>0,635 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041e\\u0441\\u0430\\u0434\\u043a\\u0430 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u043e\\u043c \\u043c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f\\r\\n\\t<\\/th>\\r\\n\\t<td>0,320 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041a\\u0438\\u043b\\u0435\\u0432\\u0430\\u0442\\u043e\\u0441\\u0442\\u044c \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u043d\\u0430 \\u0442\\u0440\\u0430\\u043d\\u0446\\u0435\\r\\n\\t<\\/th>\\r\\n\\t<td>15,3 \\u0433\\u0440\\u0430\\u0434.\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041a\\u0438\\u043b\\u0435\\u0432\\u0430\\u0442\\u043e\\u0441\\u0442\\u044c \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u043d\\u0430 \\u043c\\u0438\\u0434\\u0435\\u043b\\u0435<\\/th>\\r\\n\\t<td>18,3 \\u0433\\u0440\\u0430\\u0434.\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table><table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<th>\\u041c\\u0430\\u0441\\u0441\\u0430 \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u043d\\u043e\\u0433\\u043e \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>400 \\u043a\\u0433\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041f\\u043e\\u043b\\u0435\\u0437\\u043d\\u0430\\u044f \\u043d\\u0430\\u0433\\u0440\\u0443\\u0437\\u043a\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>625 \\u043a\\u0433\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0414\\u043e\\u043f\\u0443\\u0441\\u0442\\u0438\\u043c\\u043e\\u0435 \\u043a\\u043e\\u043b\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0447\\u0435\\u043b\\u043e\\u0432\\u0435\\u043a \\u043d\\u0430 \\u0431\\u043e\\u0440\\u0442\\u0443\\r\\n\\t<\\/th>\\r\\n\\t<td>5 \\u0447\\u0435\\u043b.\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0412\\u044b\\u0441\\u043e\\u0442\\u0430 \\u0442\\u0440\\u0430\\u043d\\u0446\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>0,510 \\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>Min. \\u0434\\u043e\\u043f\\u0443\\u0441\\u0442\\u0438\\u043c\\u0430\\u044f \\u043c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c \\u0434\\u0432\\u0438\\u0433\\u0430\\u0442\\u0435\\u043b\\u044f\\r\\n\\t<\\/th>\\r\\n\\t<td>60 \\u043b.\\u0441.\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>Max. \\u0434\\u043e\\u043f\\u0443\\u0441\\u0442\\u0438\\u043c\\u0430\\u044f \\u043c\\u043e\\u0449\\u043d\\u043e\\u0441\\u0442\\u044c \\u0434\\u0432\\u0438\\u0433\\u0430\\u0442\\u0435\\u043b\\u044f\\r\\n\\t<\\/th>\\r\\n\\t<td>75  \\u043b.\\u0441.\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041e\\u0431\\u044a\\u0435\\u043c \\u0442\\u043e\\u043f\\u043b\\u0438\\u0432\\u043d\\u043e\\u0433\\u043e \\u0431\\u0430\\u043a\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>60 \\u043b.<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>\\u0410\\u041c\\u0413- 5\\u041c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0422\\u043e\\u043b\\u0449\\u0438\\u043d\\u0430 \\u043e\\u0431\\u0448\\u0438\\u0432\\u043a\\u0438 \\u0434\\u043d\\u0438\\u0449\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>3.0 \\u043c\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0422\\u043e\\u043b\\u0449\\u0438\\u043d\\u0430 \\u043e\\u0431\\u0448\\u0438\\u0432\\u043a\\u0438 \\u0431\\u043e\\u0440\\u0442\\u0430\\r\\n\\t<\\/th>\\r\\n\\t<td>3.0 \\u043c\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041a\\u0440\\u0435\\u0439\\u0441\\u0435\\u0440\\u0441\\u043a\\u0430\\u044f \\u0441\\u043a\\u043e\\u0440\\u043e\\u0441\\u0442\\u044c \\u0441\\r\\n\\t<\\/th>\\r\\n\\t<td>\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u0434\\u0432\\u0438\\u0433\\u0430\\u0442\\u0435\\u043b\\u0435\\u043c 60 \\u043b.\\u0441. \\u0438 \\u044d\\u043a\\u0438\\u043f\\u0430\\u0436\\u0435\\u043c 2 \\u0447\\u0435\\u043b.\\r\\n\\t<\\/th>\\r\\n\\t<td>55 \\u043a\\u043c\\/\\u0447\\u0430\\u0441\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<th>\\u041e\\u0431\\u044a\\u0435\\u043c \\u0431\\u043b\\u043e\\u043a\\u043e\\u0432 \\u043f\\u043b\\u0430\\u0432\\u0443\\u0447\\u0435\\u0441\\u0442\\u0438\\r\\n\\t<\\/th>\\r\\n\\t<td>0,92 \\u043a\\u0443\\u0431.\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>","include":"<table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td>\\u0426\\u0435\\u043b\\u044c\\u043d\\u043e\\u0441\\u0432\\u0430\\u0440\\u043d\\u043e\\u0439 \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u044b\\u0439 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441 (\\u0437\\u0430\\u043f\\u0435\\u043d\\u0435\\u043d\\u043d\\u044b\\u0439)\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u041f\\u0440\\u0438\\u0432\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0431\\u0440\\u0443\\u0441 \\u0438\\u0437 \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u044f\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u041e\\u043a\\u0440\\u0430\\u0441\\u043a\\u0430 \\u043a\\u0430\\u0442\\u0435\\u0440\\u0430 \\u0441 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u043e\\u043c\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0412\\u0435\\u0442\\u0440\\u043e\\u0432\\u043e\\u0435 \\u0441\\u0442\\u0435\\u043a\\u043b\\u043e \\u0432 \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u0435\\u0432\\u043e\\u043c \\u043f\\u0440\\u043e\\u0444\\u0438\\u043b\\u0435\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u041a\\u043e\\u043d\\u0441\\u043e\\u043b\\u0438 \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u044f \\u0438\\u0437 \\u043f\\u043b\\u0430\\u0441\\u0442\\u0438\\u043a\\u0430\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0420\\u0443\\u043d\\u0434\\u0443\\u043a\\u0438 \\u0441 \\u043b\\u044e\\u043a\\u0430\\u043c\\u0438\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u041e\\u0433\\u043d\\u0438 \\u0445\\u043e\\u0434\\u043e\\u0432\\u044b\\u0435 + \\u043e\\u0433\\u043e\\u043d\\u044c \\u0441\\u0442\\u043e\\u044f\\u043d\\u043e\\u0447\\u043d\\u044b\\u0439 + \\u043a\\u043b\\u043e\\u0442\\u0438\\u043a\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430 \\u043c\\u0435\\u0445\\u0430\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0433\\u043e \\u0440\\u0443\\u043b\\u0435\\u0432\\u043e\\u0433\\u043e \\u0443\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u044f (\\u0440\\u0443\\u043b\\u044c, \\u0440\\u0435\\u0434\\u0443\\u043a\\u0442\\u043e\\u0440, \\u0442\\u0440\\u043e\\u0441)\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n\\r\\n<\\/tbody>\\r\\n<\\/table><table>\\r\\n<tbody>\\r\\n<tr>\\r\\n\\t<td>\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043f\\u043e\\u043c\\u043f\\u0430 \\u0434\\u043b\\u044f \\u043e\\u0442\\u043a\\u0430\\u0447\\u043a\\u0438 \\u0432\\u043e\\u0434\\u044b\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n\\r\\n<tr>\\r\\n\\t<td>\\u0422\\u043e\\u043f\\u043b\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0444\\u0438\\u043b\\u044c\\u0442\\u0440\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0421\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u0441\\u0438\\u0434\\u0435\\u043d\\u0438\\u044f\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0414\\u0438\\u0432\\u0430\\u043d \\u0441 \\u043c\\u044f\\u0433\\u043a\\u0438\\u043c\\u0438 \\u043d\\u0430\\u043a\\u043b\\u0430\\u0434\\u043a\\u0430\\u043c\\u0438 \\u0434\\u043b\\u044f \\u043f\\u0430\\u0441\\u0441\\u0430\\u0436\\u0438\\u0440\\u043e\\u0432\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0423\\u0442\\u043a\\u0438 \\u0448\\u0432\\u0430\\u0440\\u0442\\u043e\\u0432\\u044b\\u0435 4 \\u0448\\u0442\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u041f\\u0430\\u043d\\u0435\\u043b\\u044c \\u043f\\u0435\\u0440\\u0435\\u043a\\u043b\\u044e\\u0447\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439 \\u0441 \\u0440\\u043e\\u0437\\u0435\\u0442\\u043a\\u043e\\u0439\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0412\\u044b\\u043a\\u043b\\u044e\\u0447\\u0430\\u0442\\u0435\\u043b\\u044c \\u043c\\u0430\\u0441\\u0441\\u044b\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<tr>\\r\\n\\t<td>\\u0421\\u0442\\u0430\\u0446\\u0438\\u043e\\u043d\\u0430\\u0440\\u043d\\u044b\\u0439 \\u0442\\u043e\\u043f\\u043b\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0431\\u0430\\u043a \\u043d\\u0430 60 \\u043b \\u0441 \\u0443\\u043a\\u0430\\u0437\\u0430\\u0442\\u0435\\u043b\\u0435\\u043c \\u0443\\u0440\\u043e\\u0432\\u043d\\u044f \\u0442\\u043e\\u043f\\u043b\\u0438\\u0432\\u0430\\r\\n\\t<\\/td>\\r\\n<\\/tr>\\r\\n<\\/tbody>\\r\\n<\\/table>","width":"1.85 \\u043c.","length":"4.70 \\u043c.","people":"5 \\u0447\\u0435\\u043b.","engine":"60 \\u043b.\\u0441.","tank":"65 \\u043b","weight":"415 \\u043a\\u0433"}', 'catalog/aluma-fish-4-7-decor-1-776d69e228.jpg', 'fish-47', 1496299178, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_catalog_item_data`
--

DROP TABLE IF EXISTS `easyii_catalog_item_data`;
CREATE TABLE `easyii_catalog_item_data` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_catalog_item_data`
--

INSERT INTO `easyii_catalog_item_data` (`id`, `item_id`, `name`, `value`) VALUES
  (388, 1, 'weight', '415 кг'),
  (381, 1, 'characteristics', '<table>\r\n<tbody>\r\n<tr>\r\n	<th>Тип изготовления корпуса\r\n	</th>\r\n	<td>цельносварной\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Тип корпуса\r\n	</th>\r\n	<td>открытый</td>\r\n</tr>\r\n<tr>\r\n	<th>Длина корпуса наибольшая\r\n	</th>\r\n	<td>5,1 м </td>\r\n</tr>\r\n<tr>\r\n	<th>Габаритная длина судна\r\n	</th>\r\n	<td>4,76 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Ширина корпуса наибольшая\r\n	</th>\r\n	<td>2,27 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Габаритная ширина судна\r\n	</th>\r\n	<td>2,27 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Ширина по скуле\r\n	</th>\r\n	<td>1,70 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Высота борта на миделе\r\n	</th>\r\n	<td>0,99 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Высота надводного борта на миделе\r\n	</th>\r\n	<td>0,76 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Высота борта в кокпите\r\n	</th>\r\n	<td>0,635 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Осадка корпусом максимальная\r\n	</th>\r\n	<td>0,320 м\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Килеватость корпуса на транце\r\n	</th>\r\n	<td>15,3 град.\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<th>Килеватость корпуса на миделе</th>\r\n	<td>18,3 град.\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table><table>\r\n<tbody>\r\n<tr>\r\n	<th>Масса'),
  (387, 1, 'tank', '65 л'),
  (386, 1, 'engine', '60 л.с.'),
  (385, 1, 'people', '5 чел.'),
  (382, 1, 'include', '<table>\r\n<tbody>\r\n<tr>\r\n	<td>Цельносварной алюминиевый корпус (запененный)\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Привальный брус из алюминия\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Окраска катера с дизайном\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Ветровое стекло в алюминиевом профиле\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Консоли управления из пластика\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Рундуки с люками\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Огни ходовые + огонь стояночный + клотик\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Система механического рулевого управления (руль, редуктор, трос)\r\n	</td>\r\n</tr>\r\n\r\n</tbody>\r\n</table><table>\r\n<tbody>\r\n<tr>\r\n	<td>Электрическая помпа для откачки воды\r\n	</td>\r\n</tr>\r\n\r\n<tr>\r\n	<td>Топливный фильтр\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Съемные сидения\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Диван с мягкими накладками для пассажиров\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Утки швартовые 4 шт\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Панель переключателей с розеткой\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Выключатель массы\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Стационарный топливный бак на 60 л с указателем уровня топлива\r\n	</td>\r\n</t'),
  (383, 1, 'width', '1.85 м.'),
  (384, 1, 'length', '4.70 м.'),
  (380, 1, 'video2', 'VQLeynaTFdI'),
  (379, 1, 'video', 'oCORlhdkPTs');

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_entity_categories`
--

DROP TABLE IF EXISTS `easyii_entity_categories`;
CREATE TABLE `easyii_entity_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `fields` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `cache` tinyint(1) NOT NULL DEFAULT '1',
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_entity_categories`
--

INSERT INTO `easyii_entity_categories` (`id`, `title`, `description`, `image_file`, `fields`, `slug`, `cache`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
  (1, 'Fish 4.7', '', NULL, '{}', 'fish-47', 1, 1, 1, 10, 0, 5, 1),
  (2, 'Функциональность', '', NULL, '[{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'feature-fish-47', 1, 1, 2, 3, 1, 5, 1),
  (3, 'Надежность', '', NULL, '[{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'reliability-fish-47', 1, 1, 4, 5, 1, 5, 1),
  (4, 'Ходовые качества', '', NULL, '[{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'driving-fish-47', 1, 1, 6, 7, 1, 5, 1),
  (5, 'Видео', '', NULL, '[{"name":"slug","title":"\\u041c\\u0435\\u0442\\u043a\\u0430","type":"string","options":""},{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"youtube","title":"ID \\u0432\\u0438\\u0434\\u0435\\u043e \\u043d\\u0430 Youtube","type":"string","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'video', 1, 5, 1, 2, 0, 1, 1),
  (6, 'Материалы', '', NULL, '[{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'materials', 1, 6, 1, 2, 0, 2, 1),
  (7, 'Фото', '', NULL, '[{"name":"slug","title":"\\u041c\\u0435\\u0442\\u043a\\u0430","type":"string","options":""},{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'photo', 1, 7, 1, 2, 0, 3, 1),
  (8, 'Акции и предложения', '', NULL, '[{"name":"slug","title":"\\u041c\\u0435\\u0442\\u043a\\u0430","type":"string","options":""},{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""},{"name":"description","title":"\\u041e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0435","type":"html","options":""}]', 'offers', 1, 8, 1, 2, 0, 4, 1),
  (9, 'Характеристики', '', NULL, '[{"name":"photo","title":"\\u0424\\u043e\\u0442\\u043e","type":"file","options":""}]', 'characteristics-fish-47', 1, 1, 8, 9, 1, 5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_entity_items`
--

DROP TABLE IF EXISTS `easyii_entity_items`;
CREATE TABLE `easyii_entity_items` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `data` text NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_entity_items`
--

INSERT INTO `easyii_entity_items` (`id`, `category_id`, `title`, `data`, `order_num`, `status`) VALUES
  (1, 2, 'Носовые рундуки', '{"description":"<p>\\u041e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043d\\u043e\\u0441\\u043e\\u0432\\u044b\\u0435 \\u0440\\u0443\\u043d\\u0434\\u043a\\u0438 (\\u043e\\u0431\\u0449\\u0438\\u0439 \\u0440\\u0430\\u0437\\u043c\\u0435\\u0440 850\\u0445900\\u0445285 \\u043c\\u043c).<\\/p>","photo":"entity\\/7878787195.jpg"}', 1, 1),
  (2, 2, 'Съемные сидения', '{"description":"<p>\\u041f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u044e\\u0442 \\u043f\\u0435\\u0440\\u0435\\u043d\\u043e\\u0441\\u0438\\u0442\\u044c \\u0441\\u0438\\u0434\\u0435\\u043d\\u0438\\u044f \\u0432 \\u043b\\u044e\\u0431\\u043e\\u0435 \\u043c\\u0435\\u0441\\u0442\\u043e \\u043a\\u0430\\u0442\\u0435\\u0440\\u0430, \\u0443\\u0434\\u043e\\u0431\\u043d\\u043e\\u0435 \\u0434\\u043b\\u044f \\u0440\\u044b\\u0431\\u0430\\u043b\\u043a\\u0438 \\u0438 \\u043e\\u0442\\u0434\\u044b\\u0445\\u0430.<\\/p>","photo":"entity\\/aluma-fish-4-7-stul.jpg"}', 2, 1),
  (3, 2, 'Рундук для спиннингов', '{"description":"<p>\\u0421\\u0443\\u0445\\u043e\\u0439 \\u0440\\u0443\\u043d\\u0434\\u0443\\u043a \\u043f\\u043e\\u0434 \\u043f\\u0430\\u043b\\u0443\\u0431\\u043e\\u0439 \\u0434\\u043b\\u044f \\u0441\\u043f\\u0438\\u043d\\u043d\\u0438\\u043d\\u0433\\u043e\\u0432 \\u0438\\u043b\\u0438 \\u0434\\u0440\\u0443\\u0433\\u0438\\u0445 \\u0432\\u0435\\u0449\\u0435\\u0439.<\\/p>","photo":"entity\\/aluma-fish-4-7-runduk.jpg"}', 3, 1),
  (4, 2, 'Складные диваны', '{"description":"<p>\\u0423\\u0434\\u043e\\u0431\\u0441\\u0442\\u0432\\u043e \\u043f\\u0440\\u0438 \\u043b\\u043e\\u0432\\u043b\\u0435 \\u0440\\u044b\\u0431\\u044b, \\u043f\\u0440\\u043e\\u0441\\u0442\\u043e\\u0440\\u043d\\u043e\\u0435 \\u043c\\u0435\\u0441\\u0442\\u043e \\u0434\\u043b\\u044f \\u0441\\u043d\\u0430 \\u0434\\u0432\\u0430 \\u043f\\u043e\\u043b\\u0435\\u0437\\u043d\\u044b\\u0445 \\u043e\\u0431\\u044a\\u0435\\u043c\\u0430 \\u043f\\u043e\\u0434 \\u0441\\u0438\\u0434\\u0435\\u043d\\u044c\\u044f\\u043c\\u0438.<\\/p>","photo":"entity\\/funkcional-3165390.jpg"}', 4, 1),
  (5, 2, 'Пенополиуретан', '{"description":"<p>\\u0417\\u0430\\u043a\\u0440\\u044b\\u0442\\u043e\\u044f\\u0447\\u0435\\u0438\\u0441\\u0442\\u044b\\u0439 \\u0436\\u0435\\u0441\\u0442\\u043a\\u0438\\u0439 \\u043f\\u0435\\u043d\\u043e\\u043f\\u043e\\u043b\\u0438\\u0443\\u0440\\u0435\\u0442\\u0430\\u043d.<\\/p>","photo":"entity\\/aluma-fish-4-7-penopoliuretan.jpg"}', 6, 1),
  (6, 2, 'Сухой рундук', '{"description":"<p>\\u0411\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439 \\u0441\\u0443\\u0445\\u043e\\u0439 \\u0440\\u0443\\u043d\\u0434\\u0443\\u043a \\u043f\\u043e\\u0434 \\u043a\\u043e\\u0440\\u043c\\u043e\\u0432\\u044b\\u043c \\u0434\\u0432\\u0438\\u0430\\u043d\\u043e\\u043c.<\\/p>","photo":"entity\\/aluma-fish-4-7-runduk-dry.jpg"}', 5, 1),
  (7, 3, 'Непотопляемый сверхпрочный корпус', '{"description":"<p>\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u0441\\u0432\\u0430\\u0440\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0442\\u0440\\u0443\\u043a\\u0446\\u0438\\u044f \\u0434\\u043b\\u044f \\u0443\\u0441\\u0442\\u043e\\u0439\\u0447\\u0438\\u0432\\u043e\\u0441\\u0442\\u0438 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u043a \\u043c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u044b\\u043c \\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u043c \\u0443\\u0434\\u0430\\u0440\\u043d\\u044b\\u043c \\u043d\\u0430\\u0433\\u0440\\u0443\\u0437\\u043a\\u0430\\u043c\\r\\n<\\/p>","photo":"entity\\/silovoy-4-metall128.jpg"}', 7, 1),
  (8, 3, 'Закрытоячеистый жесткий пенополиуретан', '{"description":"<p>\\u0417\\u0430\\u043b\\u0438\\u0442\\u044b\\u0439 \\u0432 \\u0441\\u0435\\u043a\\u0446\\u0438\\u0438 \\u0441\\u0438\\u043b\\u043e\\u0432\\u043e\\u0433\\u043e \\u043d\\u0430\\u0431\\u043e\\u0440\\u0430 \\u043e\\u0431\\u0435\\u0441\\u043f\\u0435\\u0447\\u0438\\u0432\\u0430\\u0435\\u0442 \\u043d\\u0435\\u043f\\u043e\\u0442\\u043e\\u043f\\u043b\\u044f\\u0435\\u043c\\u043e\\u0441\\u0442\\u044c \\u043f\\u0440\\u0438 \\u043f\\u043e\\u043b\\u043d\\u043e\\u043c \\u0437\\u0430\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u0438\\u0438 \\u0441\\u0443\\u0434\\u043d\\u0430 \\u0432\\u043e\\u0434\\u043e\\u0439 \\u0434\\u0430\\u0436\\u0435 \\u0441 \\u043c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0437\\u0430\\u0433\\u0440\\u0443\\u0437\\u043a\\u043e\\u0439\\r\\n<\\/p>","photo":"entity\\/pena140.jpg"}', 8, 1),
  (9, 4, 'Быстрый выход на глиссер', '{"description":"<p><i>\\u0412\\u044b\\u0445\\u043e\\u0434 \\u043d\\u0430 \\u0433\\u043b\\u0438\\u0441\\u0441\\u0435\\u0440 \\u0441 \\u043f\\u043e\\u043b\\u043d\\u043e\\u0439 \\u0437\\u0430\\u0433\\u0440\\u0443\\u0437\\u043a\\u043e\\u0439 \\u0438 \\u043c\\u0430\\u043b\\u043e\\u043c\\u043e\\u0449\\u043d\\u044b\\u043c \\u043c\\u043e\\u0442\\u043e\\u0440\\u043e\\u043c<\\/i><\\/p>","photo":"entity\\/aluma-fish-4-7-glisser.jpg"}', 9, 1),
  (10, 4, 'Устойчивость на курсе', '{"description":"<p><i>\\u0413\\u0435\\u043e\\u043c\\u0435\\u0442\\u0440\\u0438\\u044f \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u0443\\u0432\\u0435\\u0440\\u0435\\u043d\\u043d\\u043e \\u0440\\u0430\\u0441\\u0441\\u0435\\u043a\\u0430\\u0442\\u044c \\u043b\\u044e\\u0431\\u044b\\u0435 \\u0432\\u043e\\u043b\\u043d\\u044b \\u0438 \\u0442\\u0432\\u0435\\u0440\\u0434\\u043e \\u0434\\u0435\\u0440\\u0436\\u0430\\u0442\\u044c \\u043a\\u0443\\u0440\\u0441<\\/i><\\/p>","photo":"entity\\/aluma-fish-4-7-kurs.jpg"}', 10, 1),
  (11, 4, 'Непревзойденная остойчивость', '{"description":"<p>\\u0421\\u043e\\u0432\\u043c\\u0435\\u0441\\u0442\\u043d\\u044b\\u0439 \\u0440\\u0435\\u0437\\u0443\\u043b\\u044c\\u0442\\u0430\\u0442 \\u043a\\u043e\\u043c\\u043f\\u044c\\u044e\\u0442\\u0435\\u0440\\u043d\\u043e\\u0433\\u043e \\u043c\\u043e\\u0434\\u0435\\u043b\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u044f \\u0438 \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u0438\\u0441\\u043f\\u044b\\u0442\\u0430\\u043d\\u0438\\u0439\\r\\n<\\/p>","photo":"entity\\/aluma-fish-4-7-ostoychivost.jpg"}', 11, 1),
  (12, 5, 'О компании', '{"slug":"about","youtube":"OFxem5H15QM","description":"<p style=\\"text-align: center;\\"><strong>\\u0422\\u0440\\u0430\\u0434\\u0438\\u0446\\u0438\\u0438,\\r\\n\\u0438\\u043d\\u043d\\u043e\\u0432\\u0430\\u0446\\u0438\\u0438,\\r\\n\\u0433\\u0430\\u0440\\u043c\\u043e\\u043d\\u0438\\u044f \\r\\n<\\/strong><\\/p><p style=\\"text-align: center;\\"><strong>\\u0427\\u0435\\u043b\\u043e\\u0432\\u0435\\u043a\\r\\n\\u0432\\u0441\\u0435\\u0433\\u0434\\u0430 \\u0441\\u0442\\u0440\\u0435\\u043c\\u0438\\u043b\\u0441\\u044f\\r\\n\\u0436\\u0438\\u0442\\u044c \\u0443 \\u0432\\u043e\\u0434\\u044b.<\\/strong><\\/p><p style=\\"text-align: justify;\\">\\u0412\\u044b\\r\\n\\u043e\\u0431\\u0440\\u0430\\u0442\\u0438\\u043b\\u0438 \\u0432\\u043d\\u0438\\u043c\\u0430\\u043d\\u0438\\u0435,\\r\\n\\u043a\\u0430\\u043a \\u0447\\u0430\\u0441\\u0442\\u043e \\u043c\\u044b\\r\\n\\u0438\\u0434\\u0435\\u043d\\u0442\\u0438\\u0444\\u0438\\u0446\\u0438\\u0440\\u0443\\u0435\\u043c\\r\\n\\u0441\\u0435\\u0431\\u044f \\u043d\\u0435 \\u043f\\u0440\\u043e\\u0441\\u0442\\u043e\\r\\n\\u043f\\u043e \\u043c\\u0435\\u0441\\u0442\\u0443 \\u0436\\u0438\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432\\u0430,\\r\\n\\u043d\\u043e \\u043f\\u043e \\u0438\\u043c\\u0435\\u043d\\u0438\\r\\n\\u0440\\u0435\\u043a\\u0438, \\u043c\\u043e\\u0440\\u044f, \\u043e\\u0437\\u0435\\u0440\\u0430,\\r\\n\\u0440\\u044f\\u0434\\u043e\\u043c \\u0441 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u043c\\r\\n\\u0436\\u0438\\u0432\\u0435\\u043c. \\u00ab\\u041c\\u044b \\u0441\\r\\n\\u0414\\u043e\\u043d\\u0430\\u00bb, \\u00ab\\u043c\\u044b \\u0441\\r\\n\\u041a\\u0443\\u0431\\u0430\\u043d\\u0438\\u00bb, \\u00ab\\u043c\\u044b\\r\\n\\u0441 \\u0412\\u043e\\u043b\\u0433\\u0438\\u00bb, \\u00ab\\u043c\\u044b\\r\\n\\u0441 \\u0411\\u0430\\u0439\\u043a\\u0430\\u043b\\u0430\\u00bb\\u2026\\r\\n\\u0412\\u0435\\u043b\\u0438\\u043a\\u0438\\u0435 \\u0432\\u043e\\u0434\\u043d\\u044b\\u0435\\r\\n\\u0430\\u0440\\u0442\\u0435\\u0440\\u0438\\u0438 \\u044f\\u0432\\u043b\\u044f\\u044e\\u0442\\u0441\\u044f\\r\\n\\u043d\\u0435 \\u043f\\u0440\\u043e\\u0441\\u0442\\u043e \\u0438\\u0441\\u0442\\u043e\\u0447\\u043d\\u0438\\u043a\\u043e\\u043c\\r\\n\\u0436\\u0438\\u0437\\u043d\\u0438. \\u041e\\u043d\\u0438 - \\u0436\\u0438\\u0432\\u044b\\u0435\\r\\n\\u043f\\u0430\\u043c\\u044f\\u0442\\u043d\\u0438\\u043a\\u0438\\r\\n\\u043f\\u0440\\u0438\\u0440\\u043e\\u0434\\u044b, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u043c\\u0438\\r\\n\\u043c\\u044b \\u043f\\u043e \\u043f\\u0440\\u0430\\u0432\\u0443\\r\\n\\u0433\\u043e\\u0440\\u0434\\u0438\\u043c\\u0441\\u044f.<\\/p><p style=\\"text-align: justify;\\">\\u041a\\u0430\\u043a\\r\\n\\u043c\\u043d\\u043e\\u0433\\u043e \\u0440\\u0430\\u0437\\u043d\\u044b\\u0445\\r\\n\\u0441\\u0443\\u0434\\u043e\\u0432 \\u0441\\u0442\\u043e\\u044f\\u0442\\r\\n\\u0432 \\u043f\\u043e\\u0440\\u0442\\u0430\\u0445 \\u0438 \\u0443\\r\\n\\u043f\\u0438\\u0440\\u0441\\u043e\\u0432 \\u0432 \\u0435\\u0432\\u0440\\u043e\\u043f\\u0435\\u0439\\u0441\\u043a\\u0438\\u0445\\r\\n\\u0433\\u043e\\u0440\\u043e\\u0434\\u0430\\u0445? \\u0422\\u044b\\u0441\\u044f\\u0447\\u0438!\\r\\n\\u0414\\u0435\\u0441\\u044f\\u0442\\u043a\\u0438 \\u0442\\u044b\\u0441\\u044f\\u0447!\\r\\n\\u0421\\u0430\\u043c\\u044b\\u0445 \\u0440\\u0430\\u0437\\u043d\\u044b\\u0445.\\r\\n\\u041e\\u0442 \\u043d\\u0435\\u0431\\u043e\\u043b\\u044c\\u0448\\u0438\\u0445\\r\\n\\u043c\\u043e\\u0442\\u043e\\u0440\\u043d\\u044b\\u0445 \\u043b\\u043e\\u0434\\u043e\\u043a,\\r\\n\\u0434\\u043e \\u043a\\u0430\\u0442\\u0435\\u0440\\u043e\\u0432\\r\\n\\u043f\\u0440\\u0435\\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0445\\r\\n\\u044f\\u0445\\u0442, \\u043d\\u0430 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445\\r\\n\\u043c\\u043e\\u0436\\u043d\\u043e \\u0441\\u043e\\u0432\\u0435\\u0440\\u0448\\u0430\\u0442\\u044c\\r\\n\\u0434\\u0430\\u043b\\u044c\\u043d\\u0438\\u0435 \\u043f\\u0443\\u0442\\u0435\\u0448\\u0435\\u0441\\u0442\\u0432\\u0438\\u044f\\r\\n\\u0438\\u043b\\u0438 \\u043f\\u0440\\u043e\\u0441\\u0442\\u043e\\r\\n\\u0436\\u0438\\u0442\\u044c. \\r\\n<\\/p><p style=\\"text-align: justify;\\"><a name=\\"_GoBack\\"><\\/a>\\r\\n\\u041e\\u0434\\u043d\\u0430\\u0436\\u0434\\u044b \\u043c\\u044b\\r\\n\\u0437\\u0430\\u0434\\u0430\\u043b\\u0438 \\u0441\\u0435\\u0431\\u0435\\r\\n\\u0432\\u043e\\u043f\\u0440\\u043e\\u0441. \\u041f\\u043e\\u0447\\u0435\\u043c\\u0443\\r\\n\\u0443 \\u043d\\u0430\\u0441 \\u043d\\u0435\\u0442 \\u0442\\u0430\\u043a?\\r\\n\\u0418 \\u043d\\u0430\\u0448\\u043b\\u0438 \\u043e\\u0442\\u0432\\u0435\\u0442.\\r\\n\\u041f\\u043e\\u0442\\u043e\\u043c\\u0443 \\u0447\\u0442\\u043e\\r\\n\\u043d\\u0438\\u043a\\u0442\\u043e \\u0432 \\u0420\\u043e\\u0441\\u0441\\u0438\\u0438\\r\\n\\u043d\\u0435 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\r\\n\\u043a\\u0430\\u0442\\u0435\\u0440\\u0430, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435\\r\\n\\u043c\\u043e\\u0433\\u0443\\u0442 \\u0441\\u043e\\u043e\\u0442\\u0432\\u0435\\u0442\\u0441\\u0442\\u0432\\u043e\\u0432\\u0430\\u0442\\u044c\\r\\n\\u043b\\u0443\\u0447\\u0448\\u0438\\u043c \\u043c\\u0438\\u0440\\u043e\\u0432\\u044b\\u043c\\r\\n\\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430\\u043c,\\r\\n\\u0443\\u0434\\u043e\\u0432\\u043b\\u0435\\u0442\\u0432\\u043e\\u0440\\u0438\\u0442\\u044c\\r\\n\\u0432\\u0437\\u044b\\u0441\\u043a\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0435\\r\\n\\u0432\\u043a\\u0443\\u0441\\u044b \\u0438 \\u0431\\u044b\\u0442\\u044c\\r\\n\\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u044b\\u043c\\u0438\\r\\n\\u043f\\u043e \\u0446\\u0435\\u043d\\u0435. \\r\\n<\\/p><p style=\\"text-align: justify;\\">\\u0418 \\u0442\\u043e\\u0433\\u0434\\u0430\\r\\n\\u043c\\u044b \\u0441\\u043e\\u0437\\u0434\\u0430\\u043b\\u0438\\r\\n\\u00ab\\u0410\\u043b\\u044e\\u043c\\u0443\\u00bb!<\\/p><p style=\\"text-align: justify;\\">\\u00ab\\u0410\\u043b\\u044e\\u043c\\u0430\\u00bb\\r\\n- \\u044d\\u0442\\u043e \\u0446\\u0435\\u043b\\u0430\\u044f \\u043b\\u0438\\u043d\\u0435\\u0439\\u043a\\u0430\\r\\n\\u0441\\u0443\\u0434\\u043e\\u0432. \\u041e\\u0442 \\u043d\\u0435\\u0431\\u043e\\u043b\\u044c\\u0448\\u0438\\u0445\\r\\n\\u0434\\u043b\\u044f \\u0440\\u044b\\u0431\\u0430\\u043b\\u043a\\u0438\\r\\n\\u0438 \\u0432\\u043e\\u0434\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0433\\u0443\\u043b\\u043e\\u043a,\\r\\n\\u0434\\u043e \\u043d\\u0430\\u0441\\u0442\\u043e\\u044f\\u0449\\u0438\\u0445\\r\\n\\u043a\\u0440\\u0443\\u043f\\u043d\\u044b\\u0445 \\u043a\\u0430\\u0442\\u0435\\u0440\\u043e\\u0432\\r\\n\\u0441 \\u043a\\u0430\\u0431\\u0438\\u043d\\u043e\\u0439, \\u043a\\u0430\\u044e\\u0442\\u043e\\u0439,\\r\\n\\u0432 \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0445 \\u043c\\u043e\\u0436\\u043d\\u043e\\r\\n\\u0436\\u0438\\u0442\\u044c \\u0438 \\u043e\\u0442\\u0434\\u044b\\u0445\\u0430\\u0442\\u044c,\\r\\n\\u043a\\u043e\\u043c\\u0444\\u043e\\u0440\\u0442\\u043d\\u043e\\r\\n\\u0440\\u0430\\u0437\\u043c\\u0435\\u0441\\u0442\\u0438\\u0442\\u044c\\u0441\\u044f\\r\\n\\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0435\\u0439.\\r\\n\\u041d\\u0435\\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e\\r\\n\\u043e\\u0442 \\u043a\\u043b\\u0430\\u0441\\u0441\\u0430 \\u0438\\u0445\\r\\n\\u0432\\u0441\\u0435\\u0445 \\u043e\\u0431\\u044a\\u0435\\u0434\\u0438\\u043d\\u044f\\u0435\\u0442\\r\\n\\u043e\\u0434\\u043d\\u043e \\u2013 \\u0432\\u044b\\u0441\\u043e\\u043a\\u0438\\u0435\\r\\n\\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438,\\r\\n\\u0441\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u044b\\u0435\\r\\n\\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u044b,\\r\\n\\u0438\\u0437\\u044b\\u0441\\u043a\\u0430\\u043d\\u043d\\u044b\\u0439\\r\\n\\u0434\\u0438\\u0437\\u0430\\u0439\\u043d, \\u0431\\u0435\\u0437\\u0443\\u043f\\u0440\\u0435\\u0447\\u043d\\u043e\\u0435\\r\\n\\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e.<\\/p><p style=\\"text-align: justify;\\">\\u0411\\u043e\\u043b\\u0435\\u0435\\r\\n15 \\u043b\\u0435\\u0442 \\u043d\\u0430\\u0448\\u0430 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f\\r\\n\\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\r\\n\\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u044e \\u0438\\u0437\\r\\n\\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u044f. \\u041c\\u044b\\r\\n\\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u043c \\u0441\\r\\n\\u043a\\u0440\\u0443\\u043f\\u043d\\u0435\\u0439\\u0448\\u0438\\u043c\\u0438\\r\\n\\u043d\\u0435\\u0444\\u0442\\u044f\\u043d\\u044b\\u043c\\u0438\\r\\n\\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f\\u043c\\u0438\\r\\n\\u0420\\u043e\\u0441\\u0441\\u0438\\u0438 \\u0438 \\u041a\\u0430\\u0437\\u0430\\u0445\\u0441\\u0442\\u0430\\u043d\\u0430\\r\\n\\u2013 \\u0441\\u0430\\u043c\\u044b\\u043c\\u0438 \\u0441\\u0442\\u0440\\u043e\\u0433\\u0438\\u043c\\u0438\\r\\n\\u0438 \\u0442\\u0440\\u0435\\u0431\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u043c\\u0438\\r\\n\\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u0430\\u043c\\u0438.\\r\\n\\u0422\\u043e\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c, \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e\\r\\n\\u0438 \\u043d\\u0430\\u0434\\u0435\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c\\r\\n\\u0437\\u0430\\u043b\\u043e\\u0436\\u0435\\u043d\\u044b \\u0432\\r\\n\\u043e\\u0441\\u043d\\u043e\\u0432\\u0443 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0430.\\r\\n<\\/p><p style=\\"text-align: justify;\\">\\u00ab\\u0410\\u043b\\u044e\\u043c\\u0430\\u00bb\\r\\n- \\u044d\\u0442\\u043e \\u0431\\u043e\\u043b\\u0435\\u0435 5 \\u0442\\u044b\\u0441\\u044f\\u0447\\r\\n\\u043a\\u0432\\u0430\\u0434\\u0440\\u0430\\u0442\\u043d\\u044b\\u0445\\r\\n\\u043c\\u0435\\u0442\\u0440\\u043e\\u0432 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445\\r\\n\\u043f\\u043b\\u043e\\u0449\\u0430\\u0434\\u0435\\u0439,\\r\\n\\u0443\\u043b\\u044c\\u0442\\u0440\\u0430\\u0441\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u044b\\u0435\\r\\n\\u0432\\u044b\\u0441\\u043e\\u043a\\u043e\\u0442\\u043e\\u0447\\u043d\\u044b\\u0435\\r\\n\\u0441\\u0442\\u0430\\u043d\\u043a\\u0438 \\u0438 \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435\\r\\n\\u043e\\u0442 \\u043b\\u0443\\u0447\\u0448\\u0438\\u0445 \\u043c\\u0438\\u0440\\u043e\\u0432\\u044b\\u0445\\r\\n\\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0435\\u0439,\\r\\n\\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439,\\r\\n\\u0432\\u044b\\u0441\\u043e\\u043a\\u043e\\u043a\\u0432\\u0430\\u043b\\u0438\\u0444\\u0438\\u0446\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0439\\r\\n\\u0438 \\u0441\\u043f\\u043b\\u043e\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439\\r\\n \\u043a\\u043e\\u043b\\u043b\\u0435\\u043a\\u0442\\u0438\\u0432. \\r\\n<\\/p><p style=\\"text-align: justify;\\">\\u0412\\u0441\\u0435\\r\\n\\u043d\\u0430\\u0448\\u0438 \\u0441\\u0443\\u0434\\u0430\\r\\n\\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u044f\\u0442\\u0441\\u044f\\r\\n\\u0438\\u0437 \\u043e\\u0441\\u043e\\u0431\\u043e\\u0433\\u043e\\r\\n\\u0432\\u044b\\u0441\\u043e\\u043a\\u043e\\u043f\\u0440\\u043e\\u0447\\u043d\\u043e\\u0433\\u043e,\\r\\n\\u043c\\u043e\\u0440\\u0441\\u043a\\u043e\\u0433\\u043e \\u0430\\u043b\\u044e\\u043c\\u0438\\u043d\\u0438\\u044f.\\r\\n\\u041e\\u043d\\u0438 \\u0441\\u043f\\u0440\\u043e\\u0435\\u043a\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u044b\\r\\n\\u043b\\u0443\\u0447\\u0448\\u0438\\u043c\\u0438 \\u0438\\u043d\\u0436\\u0435\\u043d\\u0435\\u0440\\u0430\\u043c\\u0438\\r\\n\\u0432 \\u043e\\u0431\\u043b\\u0430\\u0441\\u0442\\u0438\\r\\n\\u0433\\u0438\\u0434\\u0440\\u043e\\u0434\\u0438\\u043d\\u0430\\u043c\\u0438\\u043a\\u0438\\r\\n\\u043f\\u043e \\u0441\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439\\r\\n\\u00ab\\u043d\\u0435 \\u043f\\u043e\\u0442\\u043e\\u043f\\u043b\\u044f\\u0435\\u043c\\u043e\\u0439\\u00bb\\r\\n\\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438.\\r\\n\\u042d\\u0442\\u043e \\u043e\\u0437\\u043d\\u0430\\u0447\\u0430\\u0435\\u0442,\\r\\n\\u0447\\u0442\\u043e \\u0434\\u0430\\u0436\\u0435 \\u043f\\u043e\\u043b\\u043d\\u043e\\u0441\\u0442\\u044c\\u044e\\r\\n\\u0437\\u0430\\u043b\\u0438\\u0442\\u0430\\u044f \\u0432\\u043e\\u0434\\u043e\\u0439,\\r\\n\\u043b\\u043e\\u0434\\u043a\\u0430 \\u043e\\u0441\\u0442\\u0430\\u0435\\u0442\\u0441\\u044f\\r\\n\\u043d\\u0430 \\u043f\\u043b\\u0430\\u0432\\u0443.<\\/p><p style=\\"text-align: justify;\\">\\u0423\\u0436\\u0435\\r\\n\\u043f\\u0435\\u0440\\u0432\\u044b\\u0435 \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u044b\\r\\n\\u043f\\u043e\\u043a\\u0430\\u0437\\u0430\\u043b\\u0438, \\u0447\\u0442\\u043e\\r\\n\\u043b\\u043e\\u0434\\u043a\\u0438 \\u00ab\\u0410\\u043b\\u044e\\u043c\\u0430\\u00bb\\r\\n\\u043d\\u0430\\u043c\\u043d\\u043e\\u0433\\u043e \\u043f\\u0440\\u0435\\u0432\\u043e\\u0441\\u0445\\u043e\\u0434\\u044f\\u0442\\r\\n\\u043f\\u043e \\u0441\\u0432\\u043e\\u0438\\u043c \\u0445\\u0430\\u0440\\u0430\\u043a\\u0442\\u0435\\u0440\\u0438\\u0441\\u0442\\u0438\\u043a\\u0430\\u043c\\r\\n\\u043b\\u044e\\u0431\\u044b\\u0435 \\u0430\\u043d\\u0430\\u043b\\u043e\\u0433\\u0438\\u0447\\u043d\\u044b\\u0435\\r\\n\\u043e\\u0442\\u0435\\u0447\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0435\\r\\n\\u0441\\u0443\\u0434\\u0430 \\u0438 \\u043d\\u0438\\u0447\\u0435\\u043c\\r\\n\\u043d\\u0435 \\u0443\\u0441\\u0442\\u0443\\u043f\\u0430\\u044e\\u0442\\r\\n\\u043b\\u0443\\u0447\\u0448\\u0438\\u043c \\u043c\\u0438\\u0440\\u043e\\u0432\\u044b\\u043c\\r\\n\\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f\\u043c.\\r\\n\\u041f\\u0440\\u0438 \\u044d\\u0442\\u043e\\u043c, \\u043d\\u0430\\u0448\\u0430\\r\\n\\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u044f\\r\\n\\u043e\\u0431\\u043b\\u0430\\u0434\\u0430\\u0435\\u0442 \\u043d\\u0435\\u043e\\u0441\\u043f\\u043e\\u0440\\u0438\\u043c\\u044b\\u043c\\u0438\\r\\n\\u043f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0430\\u043c\\u0438.\\r\\n\\u0426\\u0435\\u043d\\u044b \\u043d\\u0430 \\u043a\\u0430\\u0442\\u0435\\u0440\\u0430\\r\\n\\u00ab\\u0410\\u043b\\u044e\\u043c\\u0430\\u00bb \\u043d\\u0430\\u043c\\u043d\\u043e\\u0433\\u043e\\r\\n\\u043d\\u0438\\u0436\\u0435 \\u0438\\u043c\\u043f\\u043e\\u0440\\u0442\\u043d\\u044b\\u0445\\r\\n\\u0430\\u043d\\u0430\\u043b\\u043e\\u0433\\u043e\\u0432.<\\/p><p style=\\"text-align: justify;\\"> \\u041c\\u044b\\r\\n\\u0432\\u0441\\u0435\\u0433\\u0434\\u0430 \\u0440\\u044f\\u0434\\u043e\\u043c\\r\\n\\u0441 \\u0432\\u0430\\u043c\\u0438. \\u0411\\u044b\\u0441\\u0442\\u0440\\u043e\\r\\n\\u0440\\u0435\\u0430\\u0433\\u0438\\u0440\\u0443\\u0435\\u043c \\u043d\\u0430\\r\\n\\u043b\\u044e\\u0431\\u044b\\u0435 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f\\r\\n\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u044f\\r\\n\\u0438 \\u043e\\u0440\\u0438\\u0435\\u043d\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u044b\\r\\n\\u0438\\u043c\\u0435\\u043d\\u043d\\u043e \\u043d\\u0430 \\u043d\\u0430\\u0448\\u0435\\u0433\\u043e,\\r\\n\\u043e\\u0442\\u0435\\u0447\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u043e\\u0433\\u043e\\r\\n\\u043f\\u043e\\u0442\\u0440\\u0435\\u0431\\u0438\\u0442\\u0435\\u043b\\u044f.\\r\\n<\\/p>","photo":""}', 12, 1),
  (13, 5, 'Технологии', '{"slug":"tech","youtube":"OFxem5H15QM","description":"<p>\\u041f\\u0440\\u043e\\u043c\\u044b\\u0448\\u043b\\u0435\\u043d\\u043d\\u043e\\u0435 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u043e \\u0441 \\u0432\\u044b\\u0441\\u043e\\u043a\\u043e\\u0442\\u043e\\u0447\\u043d\\u044b\\u043c \\u0441\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u044b\\u043c \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435\\u043c.<\\/p>","photo":"entity\\/img0159.jpg"}', 13, 1),
  (14, 6, 'Качественный пенополиуретан', '{"description":"<p>\\u0416\\u0435\\u0441\\u0442\\u043a\\u0438\\u0439 \\u0437\\u0430\\u043a\\u0440\\u044b\\u0442\\u043e\\u044f\\u0447\\u0435\\u0438\\u0441\\u0442\\u044b\\u0439 \\u043f\\u0435\\u043d\\u043e\\u043f\\u043e\\u043b\\u0438\\u0443\\u0440\\u0435\\u0442\\u0430\\u043d, \\u0441\\u0435\\u0440\\u0442\\u0438\\u0444\\u0438\\u0446\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d \\u043c\\u043e\\u0440\\u0441\\u043a\\u0438\\u043c \\u0440\\u0435\\u0433\\u0438\\u0441\\u0442\\u0440\\u043e\\u043c.<\\/p>","photo":"entity\\/logo2.png"}', 14, 1),
  (15, 6, 'Сварочные швы', '{"description":"<p \\"=\\"\\">\\u0414\\u0432\\u0443\\u0441\\u0442\\u043e\\u0440\\u043e\\u043d\\u043d\\u0438\\u0435 \\u0441\\u0432\\u0430\\u0440\\u043e\\u0447\\u043d\\u044b\\u0435 \\u0448\\u0432\\u044b, \\u0432\\u044b\\u043f\\u043e\\u043b\\u043d\\u0435\\u043d\\u043d\\u044b\\u0435 \\u043d\\u0430 \\u0432\\u044b\\u0441\\u043e\\u043a\\u043e\\u043f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u043e\\u043c \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u0438\\u0438 KEMPPI<\\/p>","photo":"entity\\/kemppilogotransparent-fad4cbaa7f.png"}', 15, 1),
  (16, 6, 'Высококачественная окраска', '{"description":"<p>\\u041e\\u043a\\u0440\\u0430\\u0441\\u043a\\u0430 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u0430 \\u0441\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0441\\u0443\\u0434\\u043e\\u0432\\u043e\\u0439 2-\\u0445 \\u043a\\u043e\\u043c\\u043f\\u043e\\u043d\\u0435\\u043d\\u0442\\u043d\\u043e\\u0439 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 HEMPEL.<\\/p>","photo":"entity\\/hem-logo-rgb.png"}', 16, 1),
  (17, 7, 'Как купить', '{"slug":"how-to-buy","description":"<h3>1. \\u041d\\u0430\\u0439\\u0442\\u0438 \\u0434\\u0435\\u043d\\u044c\\u0433\\u0438<\\/h3><h3>2. \\u041d\\u0430\\u0439\\u0442\\u0438 \\u043b\\u043e\\u0434\\u043a\\u0443<\\/h3><h3>3. \\u041e\\u0441\\u043c\\u043e\\u0442\\u0440\\u0435\\u0442\\u044c \\u043b\\u043e\\u0434\\u043a\\u0443<\\/h3><h3>4. \\u041e\\u0444\\u043e\\u0440\\u043c\\u0438\\u0442\\u044c \\u0441\\u0434\\u0435\\u043b\\u043a\\u0443<\\/h3>","photo":"entity\\/full-image-01.jpg"}', 17, 1),
  (18, 8, 'Получи тент в подарок', '{"slug":"gift","description":"<h3>\\u041e\\u0441\\u0442\\u0430\\u0432\\u044c \\u0441\\u0432\\u043e\\u0438 \\u043a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u043d\\u044b\\u0435 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0435 \\u0438 \\u043f\\u043e\\u043b\\u0443\\u0447\\u0438 \\u0442\\u0435\\u043d\\u0442 \\u0432 \\u043f\\u043e\\u0434\\u0430\\u0440\\u043e\\u043a<\\/h3>","photo":"entity\\/040960.jpg"}', 18, 1),
  (19, 8, 'Получить брошюру', '{"slug":"brochure","description":"","photo":"entity\\/portal-brochures.png"}', 19, 1),
  (20, 8, 'Акция! Тент в подарок', '{"slug":"delivery","description":"","photo":""}', 20, 1),
  (21, 8, 'Получить консультацию', '{"slug":"consultation","description":"","photo":""}', 21, 1),
  (22, 8, 'Заказать бесплатный звонок', '{"slug":"callback","description":"","photo":""}', 22, 1),
  (23, 9, 'Размер 1', '{"photo":"entity\\/photo2.jpg"}', 24, 1),
  (24, 9, 'Размер 2', '{"photo":"entity\\/photo1.jpg"}', 23, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_faq`
--

DROP TABLE IF EXISTS `easyii_faq`;
CREATE TABLE `easyii_faq` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_feedback`
--

DROP TABLE IF EXISTS `easyii_feedback`;
CREATE TABLE `easyii_feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer_subject` varchar(128) DEFAULT NULL,
  `answer_text` text,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_files`
--

DROP TABLE IF EXISTS `easyii_files`;
CREATE TABLE `easyii_files` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(11) DEFAULT '0',
  `slug` varchar(128) DEFAULT NULL,
  `downloads` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_gallery_categories`
--

DROP TABLE IF EXISTS `easyii_gallery_categories`;
CREATE TABLE `easyii_gallery_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text,
  `image_file` varchar(128) DEFAULT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `tree` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_gallery_categories`
--

INSERT INTO `easyii_gallery_categories` (`id`, `title`, `description`, `image_file`, `slug`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
  (1, 'Конструктор Fish 4.7', '', NULL, 'constructor-fish-47', 4, 2, 3, 1, 1, 1),
  (2, 'Размеры Fish 4.7', '', NULL, 'size-fish-47', 4, 4, 5, 1, 2, 1),
  (4, 'Fish 4.7', '', NULL, 'fish-47', 4, 1, 6, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_guestbook`
--

DROP TABLE IF EXISTS `easyii_guestbook`;
CREATE TABLE `easyii_guestbook` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `answer` text,
  `email` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_loginform`
--

DROP TABLE IF EXISTS `easyii_loginform`;
CREATE TABLE `easyii_loginform` (
  `id` int(11) NOT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `user_agent` varchar(1024) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_loginform`
--

INSERT INTO `easyii_loginform` (`id`, `username`, `password`, `ip`, `user_agent`, `time`, `success`) VALUES
  (1, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1495889492, 1),
  (2, 'root', 'XySoIQJo', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496237101, 0),
  (3, 'root', 'XySoIQJo', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496237110, 0),
  (4, 'root', 'XySoIQJo', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496237111, 0),
  (5, 'root', 'XySoIQJo', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496237112, 0),
  (6, 'root', 'XySoIQJo', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496237325, 0),
  (7, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496238260, 1),
  (8, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496426795, 1),
  (9, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1496642797, 1),
  (10, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1497078027, 1),
  (11, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0', 1497292922, 1),
  (12, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0', 1499025887, 1),
  (13, 'root', 'http://aluma.localhost/', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1499107214, 0),
  (14, 'root', '******', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1499107220, 1),
  (15, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499148678, 1),
  (16, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499151023, 1),
  (17, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499158577, 1),
  (18, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499170623, 1),
  (19, 'root', '******', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499251095, 1),
  (20, 'root', '******', '188.168.215.27', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0', 1499374910, 1),
  (21, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1499408048, 1),
  (22, 'root', '******', '185.102.138.140', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36', 1499709679, 1),
  (23, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', 1499778248, 1),
  (24, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0', 1500282767, 1),
  (25, 'root', '******', '212.192.204.44', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.86 Safari/537.36', 1500357385, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_menu`
--

DROP TABLE IF EXISTS `easyii_menu`;
CREATE TABLE `easyii_menu` (
  `menu_id` int(11) NOT NULL,
  `slug` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `items` text,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_menu`
--

INSERT INTO `easyii_menu` (`menu_id`, `slug`, `title`, `items`, `status`) VALUES
  (1, 'main', 'Главное меню', '[\n    {\n        "label": "Характеристики",\n        "url": "/#harakteristiki"\n    },\n    {\n        "label": "Материалы",\n        "url": "/#proizvodstvo"\n    },\n    {\n        "label": "Галерея",\n        "url": "/#galereya"\n    },\n    {\n        "label": "О компании",\n        "url": "/#o-kompanii"\n    }\n]', 1),
  (2, 'extra', 'Доп. меню', '[\n    {\n        "label": "<span>Конфигурация и стоимость</span>                     <i class=\\"fa fa-anchor\\" aria-hidden=\\"true\\"></i>",\n        "url": "#stoimost"\n    }\n]', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_migration`
--

DROP TABLE IF EXISTS `easyii_migration`;
CREATE TABLE `easyii_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `easyii_migration`
--

INSERT INTO `easyii_migration` (`version`, `apply_time`) VALUES
  ('m000000_000000_base', 1495889489),
  ('m000000_000000_install', 1495889490),
  ('m000009_100000_update', 1495889490),
  ('m000009_200000_update', 1495889491),
  ('m000009_200003_module_menu', 1495889491),
  ('m000009_200004_update', 1495889491);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_modules`
--

DROP TABLE IF EXISTS `easyii_modules`;
CREATE TABLE `easyii_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `class` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `icon` varchar(32) DEFAULT NULL,
  `settings` text,
  `notice` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_modules`
--

INSERT INTO `easyii_modules` (`id`, `name`, `class`, `title`, `icon`, `settings`, `notice`, `order_num`, `status`) VALUES
  (1, 'entity', 'yii\\easyii\\modules\\entity\\EntityModule', 'Объекты', 'asterisk', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"itemsInFolder":false}', 0, 80, 1),
  (2, 'article', 'yii\\easyii\\modules\\article\\ArticleModule', 'Статьи', 'pencil', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"articleThumb":true,"enablePhotos":true,"enableTags":true,"enableShort":true,"shortMaxLength":255,"itemsInFolder":false,"itemSlugImmutable":false}', 0, 100, 1),
  (3, 'carousel', 'yii\\easyii\\modules\\carousel\\CarouselModule', 'Карусель', 'picture', '{"enableTitle":true,"enableText":true}', 0, 40, 0),
  (4, 'catalog', 'yii\\easyii\\modules\\catalog\\CatalogModule', 'Каталог', 'list-alt', '{"categoryThumb":true,"categorySlugImmutable":false,"categoryDescription":true,"itemsInFolder":false,"itemThumb":true,"itemPhotos":true,"itemDescription":true,"itemSlugImmutable":false}', 0, 120, 1),
  (5, 'faq', 'yii\\easyii\\modules\\faq\\FaqModule', 'Вопросы и ответы', 'question-sign', '{"questionHtmlEditor":true,"answerHtmlEditor":true,"enableTags":true}', 0, 45, 0),
  (6, 'feedback', 'yii\\easyii\\modules\\feedback\\FeedbackModule', 'Обратная связь', 'earphone', '{"mailAdminOnNewFeedback":true,"subjectOnNewFeedback":"\\u041d\\u043e\\u0432\\u0430\\u044f \\u0437\\u0430\\u044f\\u0432\\u043a\\u0430 \\u043d\\u0430 \\u0441\\u0430\\u0439\\u0442\\u0435","templateOnNewFeedback":"@app\\/mail\\/new_feedback","answerTemplate":"@easyii\\/modules\\/feedback\\/mail\\/ru\\/answer","answerSubject":"Answer on your feedback message","answerHeader":"Hello,","answerFooter":"Best regards.","telegramAdminOnNewFeedback":false,"telegramTemplateOnNewFeedback":"@easyii\\/modules\\/feedback\\/telegram\\/ru\\/new_feedback","enableTitle":false,"enableEmail":true,"enablePhone":true,"enableText":true,"enableCaptcha":false}', 48, 60, 1),
  (7, 'file', 'yii\\easyii\\modules\\file\\FileModule', 'Файлы', 'floppy-disk', '{"slugImmutable":false}', 0, 30, 0),
  (8, 'gallery', 'yii\\easyii\\modules\\gallery\\GalleryModule', 'Фотогалерея', 'camera', '{"categoryThumb":true,"itemsInFolder":false,"categoryTags":true,"categorySlugImmutable":false,"categoryDescription":true}', 0, 95, 1),
  (9, 'guestbook', 'yii\\easyii\\modules\\guestbook\\GuestbookModule', 'Гостевая книга', 'book', '{"enableTitle":false,"enableEmail":true,"preModerate":false,"enableCaptcha":false,"mailAdminOnNewPost":true,"subjectOnNewPost":"New message in the guestbook.","templateOnNewPost":"@easyii\\/modules\\/guestbook\\/mail\\/en\\/new_post","frontendGuestbookRoute":"\\/guestbook","subjectNotifyUser":"Your post in the guestbook answered","templateNotifyUser":"@easyii\\/modules\\/guestbook\\/mail\\/en\\/notify_user"}', 0, 65, 0),
  (10, 'menu', 'yii\\easyii\\modules\\menu\\MenuModule', 'Меню', 'menu-hamburger', '{"slugImmutable":false}', 0, 51, 1),
  (11, 'news', 'yii\\easyii\\modules\\news\\NewsModule', 'Новости', 'bullhorn', '{"enableThumb":true,"enablePhotos":true,"enableShort":true,"shortMaxLength":256,"enableTags":true,"slugImmutable":false}', 0, 90, 0),
  (12, 'page', 'yii\\easyii\\modules\\page\\PageModule', 'Страницы', 'file', '{"slugImmutable":true,"defaultFields":"[]"}', 0, 50, 1),
  (13, 'shopcart', 'yii\\easyii\\modules\\shopcart\\ShopcartModule', 'Заказы', 'shopping-cart', '{"mailAdminOnNewOrder":true,"subjectOnNewOrder":"New order","templateOnNewOrder":"@easyii\\/modules\\/shopcart\\/mail\\/en\\/new_order","subjectNotifyUser":"Your order status changed","templateNotifyUser":"@easyii\\/modules\\/shopcart\\/mail\\/en\\/notify_user","frontendShopcartRoute":"\\/shopcart\\/order","enablePhone":true,"enableEmail":true}', 0, 70, 0),
  (14, 'subscribe', 'yii\\easyii\\modules\\subscribe\\SubscribeModule', 'E-mail рассылка', 'envelope', '[]', 0, 10, 1),
  (15, 'text', 'yii\\easyii\\modules\\text\\TextModule', 'Текстовые блоки', 'font', '[]', 0, 20, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_news`
--

DROP TABLE IF EXISTS `easyii_news`;
CREATE TABLE `easyii_news` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `image_file` varchar(128) DEFAULT NULL,
  `short` varchar(1024) DEFAULT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `views` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_pages`
--

DROP TABLE IF EXISTS `easyii_pages`;
CREATE TABLE `easyii_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `text` text,
  `slug` varchar(128) DEFAULT NULL,
  `show_in_menu` tinyint(1) DEFAULT '0',
  `fields` text,
  `data` text,
  `tree` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  `depth` int(11) DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_pages`
--

INSERT INTO `easyii_pages` (`id`, `title`, `text`, `slug`, `show_in_menu`, `fields`, `data`, `tree`, `lft`, `rgt`, `depth`, `order_num`, `status`) VALUES
  (1, 'Главная', '', 'index', 0, '[{"name":"video","title":"ID \\u0432\\u0438\\u0434\\u0435\\u043e \\u043d\\u0430 Youtube","type":"string","options":""}]', '{"video":"oCORlhdkPTs"}', 1, 1, 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_photos`
--

DROP TABLE IF EXISTS `easyii_photos`;
CREATE TABLE `easyii_photos` (
  `id` int(11) NOT NULL,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image_file` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_photos`
--

INSERT INTO `easyii_photos` (`id`, `class`, `item_id`, `image_file`, `description`, `order_num`) VALUES
  (12, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/1-cca437dfff.jpg', '', 12),
  (13, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/4-474cb9e0d3.jpg', '', 13),
  (4, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/brown-9ffd9bb97d.png', 'brown-wave', 4),
  (5, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/blue-2-fa6f2a74fc.png', 'blue-wave', 5),
  (6, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/brown-2-4219a50280.png', 'brown-stripe', 6),
  (7, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/red-2-10e1e1f91f.png', 'red-wave', 7),
  (8, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/red-e5e164cd96.png', 'red-stripe', 8),
  (9, 'yii\\easyii\\modules\\gallery\\models\\Category', 1, 'gallery/blue-1-bc65178da1.png', 'blue-stripe', 9),
  (10, 'yii\\easyii\\modules\\gallery\\models\\Category', 2, 'gallery/4-2756172792.png', '', 10),
  (11, 'yii\\easyii\\modules\\gallery\\models\\Category', 2, 'gallery/5-707e02b299.png', '', 11),
  (14, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/3-98908fce27.jpg', '', 14),
  (15, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/6-29e48b79ae.jpg', '', 15),
  (16, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/2-d71b1233f5.jpg', '', 16),
  (17, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'catalog/5-7a576629fe.jpg', '', 17);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_seotext`
--

DROP TABLE IF EXISTS `easyii_seotext`;
CREATE TABLE `easyii_seotext` (
  `id` int(11) NOT NULL,
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_seotext`
--

INSERT INTO `easyii_seotext` (`id`, `class`, `item_id`, `h1`, `title`, `keywords`, `description`) VALUES
  (1, 'yii\\easyii\\modules\\catalog\\models\\Item', 1, 'Aluma Fish 4.7', 'Моторная лодка Aluma Fish 4.7', '', 'Производство и продажа моторных лодок от производителя.'),
  (2, 'yii\\easyii\\modules\\page\\models\\Page', 1, 'Производство алюминиевых катеров', 'Производство алюминиевых катеров в Ростове-на-Дону', '', 'Производство алюминиевых катеров в Ростове-на-Дону. Доставка в любой регион РФ и стран СНГ');

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_settings`
--

DROP TABLE IF EXISTS `easyii_settings`;
CREATE TABLE `easyii_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `visibility` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_settings`
--

INSERT INTO `easyii_settings` (`id`, `name`, `title`, `value`, `visibility`) VALUES
  (1, 'easyii_version', 'EasyiiCMS version', '0.91', 0),
  (2, 'image_max_width', 'Максимальная ширина загружаемых изображений, которые автоматически не сжимаются', '1900', 2),
  (3, 'redactor_plugins', 'Список плагинов редактора Redactor через запятую', 'imagemanager, filemanager, table, fullscreen', 1),
  (4, 'ga_service_email', 'E-mail сервис аккаунта Google Analytics', '', 1),
  (5, 'ga_profile_id', 'Номер профиля Google Analytics', '', 1),
  (6, 'ga_p12_file', 'Путь к файлу ключей p12 сервис аккаунта Google Analytics', '', 1),
  (7, 'gm_api_key', 'Google Maps API ключ', '', 1),
  (8, 'recaptcha_key', 'ReCaptcha key', '', 1),
  (9, 'password_salt', 'Password salt', '67HIagWo_kCqjpCcF5ezzU1uM9l9irY2', 0),
  (10, 'root_auth_key', 'Root authorization key', 'TilnRVblrPGPK5pjbPB21cK7AMW8HfGs', 0),
  (11, 'root_password', 'Пароль разработчика', '417fc3c5d74d64616a0fc7c8e40b58768433906b', 1),
  (12, 'auth_time', 'Время авторизации', '86400', 1),
  (13, 'robot_email', 'E-mail рассыльщика', 'noreply@aluma-boats.ru', 1),
  (14, 'admin_email', 'E-mail администратора', 'info@aluma-boats.ru', 2),
  (15, 'telegram_chat_id', 'Telegram chat ID', '', 2),
  (16, 'telegram_bot_token', 'Telegram bot token', '', 1),
  (17, 'recaptcha_secret', 'ReCaptcha secret', '', 1),
  (18, 'toolbar_position', 'Позиция панели на сайте ("top" or "bottom" or "hide")', 'hide', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_shopcart_goods`
--

DROP TABLE IF EXISTS `easyii_shopcart_goods`;
CREATE TABLE `easyii_shopcart_goods` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `price` float DEFAULT '0',
  `discount` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_shopcart_orders`
--

DROP TABLE IF EXISTS `easyii_shopcart_orders`;
CREATE TABLE `easyii_shopcart_orders` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `remark` varchar(1024) DEFAULT NULL,
  `access_token` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_subscribe_history`
--

DROP TABLE IF EXISTS `easyii_subscribe_history`;
CREATE TABLE `easyii_subscribe_history` (
  `id` int(11) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `body` text,
  `sent` int(11) DEFAULT '0',
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_subscribe_history`
--

INSERT INTO `easyii_subscribe_history` (`id`, `subject`, `body`, `sent`, `time`) VALUES
  (1, 'Брошюра', '<p "="">Письмо с отправкой брошюры<br></p>', 0, 1496563376);

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_subscribe_subscribers`
--

DROP TABLE IF EXISTS `easyii_subscribe_subscribers`;
CREATE TABLE `easyii_subscribe_subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `time` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_tags`
--

DROP TABLE IF EXISTS `easyii_tags`;
CREATE TABLE `easyii_tags` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `frequency` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_tags_assign`
--

DROP TABLE IF EXISTS `easyii_tags_assign`;
CREATE TABLE `easyii_tags_assign` (
  `class` varchar(128) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `easyii_texts`
--

DROP TABLE IF EXISTS `easyii_texts`;
CREATE TABLE `easyii_texts` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `slug` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `easyii_texts`
--

INSERT INTO `easyii_texts` (`id`, `text`, `slug`) VALUES
  (1, 'г. Ростов-на-Дону, ул. 1 линия, 91', 'address'),
  (2, '8 800 770 71 94', 'phone'),
  (3, 'info@aluma-boats.ru', 'mail'),
  (4, 'https://www.facebook.com/alumaboats/', 'facebook'),
  (5, 'https://vk.com/aluma_boats', 'vkontakte'),
  (6, 'https://www.instagram.com/alumaboats/', 'instagram'),
  (7, 'https://www.youtube.com/channel/UCVSQRdvkVi12B3zwXNUBErA?guided_help_flow=3', 'youtube');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `easyii_admins`
--
ALTER TABLE `easyii_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token` (`access_token`);

--
-- Индексы таблицы `easyii_article_categories`
--
ALTER TABLE `easyii_article_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_article_items`
--
ALTER TABLE `easyii_article_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_carousel`
--
ALTER TABLE `easyii_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_catalog_categories`
--
ALTER TABLE `easyii_catalog_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_catalog_items`
--
ALTER TABLE `easyii_catalog_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_catalog_item_data`
--
ALTER TABLE `easyii_catalog_item_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id_name` (`item_id`,`name`),
  ADD KEY `value` (`value`(300));

--
-- Индексы таблицы `easyii_entity_categories`
--
ALTER TABLE `easyii_entity_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_entity_items`
--
ALTER TABLE `easyii_entity_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_faq`
--
ALTER TABLE `easyii_faq`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_feedback`
--
ALTER TABLE `easyii_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_files`
--
ALTER TABLE `easyii_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_gallery_categories`
--
ALTER TABLE `easyii_gallery_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_guestbook`
--
ALTER TABLE `easyii_guestbook`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_loginform`
--
ALTER TABLE `easyii_loginform`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_menu`
--
ALTER TABLE `easyii_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Индексы таблицы `easyii_migration`
--
ALTER TABLE `easyii_migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `easyii_modules`
--
ALTER TABLE `easyii_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_news`
--
ALTER TABLE `easyii_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_pages`
--
ALTER TABLE `easyii_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Индексы таблицы `easyii_photos`
--
ALTER TABLE `easyii_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_item` (`class`,`item_id`);

--
-- Индексы таблицы `easyii_seotext`
--
ALTER TABLE `easyii_seotext`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `model_item` (`class`,`item_id`);

--
-- Индексы таблицы `easyii_settings`
--
ALTER TABLE `easyii_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_shopcart_goods`
--
ALTER TABLE `easyii_shopcart_goods`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_shopcart_orders`
--
ALTER TABLE `easyii_shopcart_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_subscribe_history`
--
ALTER TABLE `easyii_subscribe_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `easyii_subscribe_subscribers`
--
ALTER TABLE `easyii_subscribe_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `easyii_tags`
--
ALTER TABLE `easyii_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `easyii_tags_assign`
--
ALTER TABLE `easyii_tags_assign`
  ADD KEY `class` (`class`),
  ADD KEY `item_tag` (`item_id`,`tag_id`);

--
-- Индексы таблицы `easyii_texts`
--
ALTER TABLE `easyii_texts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `easyii_admins`
--
ALTER TABLE `easyii_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_article_categories`
--
ALTER TABLE `easyii_article_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_article_items`
--
ALTER TABLE `easyii_article_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `easyii_carousel`
--
ALTER TABLE `easyii_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_categories`
--
ALTER TABLE `easyii_catalog_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_items`
--
ALTER TABLE `easyii_catalog_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_catalog_item_data`
--
ALTER TABLE `easyii_catalog_item_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=389;
--
-- AUTO_INCREMENT для таблицы `easyii_entity_categories`
--
ALTER TABLE `easyii_entity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `easyii_entity_items`
--
ALTER TABLE `easyii_entity_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `easyii_faq`
--
ALTER TABLE `easyii_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_feedback`
--
ALTER TABLE `easyii_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT для таблицы `easyii_files`
--
ALTER TABLE `easyii_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_gallery_categories`
--
ALTER TABLE `easyii_gallery_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `easyii_guestbook`
--
ALTER TABLE `easyii_guestbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_loginform`
--
ALTER TABLE `easyii_loginform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `easyii_menu`
--
ALTER TABLE `easyii_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `easyii_modules`
--
ALTER TABLE `easyii_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `easyii_news`
--
ALTER TABLE `easyii_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_pages`
--
ALTER TABLE `easyii_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_photos`
--
ALTER TABLE `easyii_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `easyii_seotext`
--
ALTER TABLE `easyii_seotext`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `easyii_settings`
--
ALTER TABLE `easyii_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `easyii_shopcart_goods`
--
ALTER TABLE `easyii_shopcart_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_shopcart_orders`
--
ALTER TABLE `easyii_shopcart_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `easyii_subscribe_history`
--
ALTER TABLE `easyii_subscribe_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `easyii_subscribe_subscribers`
--
ALTER TABLE `easyii_subscribe_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `easyii_tags`
--
ALTER TABLE `easyii_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `easyii_texts`
--
ALTER TABLE `easyii_texts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;