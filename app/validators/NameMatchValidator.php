<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 18.07.17
 * Time: 09:45
 */

namespace app\validators;


use yii\validators\RegularExpressionValidator;

class NameMatchValidator extends RegularExpressionValidator
{
    public $pattern = '/^[\s\-А-Яа-яёЁ]+$/sui';
    public $message = '{attribute} может содержать только русские буквы.';
}