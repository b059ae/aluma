<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:50
 */

namespace app\widgets\bannerButton;

use app\widgets\bannerForm\models\BannerCardForm;
use yii\base\Widget;
use yii\helpers\Url;

/**
 * Плавающая кнопка обратного звонка
 * Class BannerButton
 * @package app\widgets\callback
 */
class BannerButton extends Widget
{
    public function run()
    {
        echo $this->render('banner_button', [
            'model' => new BannerCardForm(),// Форма для заполнения
            'url' => Url::to(['/feedback/banner']), // URL на который будет произведен сабмит
            'metrika' => 'buttonHead', // Цель, которая сработает при сабмите формы
        ]);
    }
}