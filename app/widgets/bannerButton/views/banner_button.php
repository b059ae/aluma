<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:53
 */

/** @var $url string */
/** @var $model \app\widgets\callback\models\CallbackForm */
/** @var $metrika string */

$popupId = uniqid('banner_button', false);
?>
<!--Модальное окно-->
<?= $this->render('popup', [
    'popupId' => $popupId,
    'url' => $url,
    'model' => $model,
    'metrika' => $metrika,
]) ?>
<!--/Модальное окно-->
<!--Кнопка-->
<a class="button-primary fancybox" href="#<?= $popupId ?>">Узнайте адрес дилера в Вашем городе </a>
<!--/Кнопка-->
