<?php

namespace app\widgets\gift;

use yii\base\Widget;
use yii\helpers\Url;

/**
 * Кнопка, которая вызывает всплывающее окно с акцией
 */
class GiftForm extends Widget
{
    public $boat; // Название лодки

    public function run()
    {
        echo $this->render('gift', [
            'model' => new \app\widgets\gift\models\GiftForm(['boat' => $this->boat]),
            'url' => Url::to(['/feedback/gift']),
            'metrika' => 'buttonHead',
        ]);
    }
}