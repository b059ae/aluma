<?php

namespace app\widgets\gift\models;

use yii\easyii\modules\feedback\api\Feedback;
use app\models\AbstractFeedbackForm;

class GiftForm extends AbstractFeedbackForm
{
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $text = $this->text;

        if (strlen($this->boat) > 0) { // Заявка со страницы лодки
            // Название лодки
            $text .= PHP_EOL . $this->boat;
            // Корзина
            $session = \Yii::$app->session;
            // Комплектация
            $boatModel = json_decode($session->get('model', ''));
            if (strlen($boatModel) > 0) {
                $text .= PHP_EOL . $boatModel;
            }
            // Дополнительное оборудование
            $shoppingCart = json_decode($session->get('shoppingCart', '[]'));
            if (count($shoppingCart) > 0) {
                $text .= PHP_EOL . "Дополнительное оборудование:" . PHP_EOL;
                foreach ($shoppingCart as $item) {
                    $text .= $item->name . " " . $item->count . " шт." . PHP_EOL;
                }
            }
        }

        // Создание заявки в AmoCRM
        // TODO: Создание заявки в AmoCRM

        // Сохранение заявки с сайта
        try {
            $model = new Feedback();
            $model->api_save([
                'name' => $this->name,
                'phone' => $this->phone,
                'email' => $this->email,
                'text' => $text,
            ]);
        } catch (\Exception $e) {
        }

        return true;
    }
}