<?php

namespace app\widgets\gift\actions;

use app\widgets\gift\models\GiftForm;
use Yii;
use yii\base\Action;

class GiftAction extends Action
{
    /**
     * Форма Доставка в подарок
     * @return string
     */
    public function run()
    {
        $model = new GiftForm();

        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->save();
        }
        return ;
    }
}