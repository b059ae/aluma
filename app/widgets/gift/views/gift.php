<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 03.06.2017
 * Time: 16:55
 */
use yii\easyii\helpers\Upload;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var $model \app\widgets\gift\models\GiftForm */
/** @var $url mixed */
/** @var $metrika mixed */

$asset = \app\assets\AppAsset::register($this);
$title = 'Записаться на тест-драйв';
?>

<div class="order_promote_container">
    <h4>Записаться на тест-драйв</h4>
    <?php
    $formId = uniqid('gift-form', false);

    $form = \app\widgets\ajaxForm\AjaxForm::begin([
        'id' => $formId,
        'metrika' => 'buttonGift',
        'enableClientValidation' => true,
        'action' => Url::to($url),
    ]);

    // Имя
    echo $form->field($model, 'name', [
        'template' => '{input}<div class="error">{error}</div>',
    ])->textInput([
        'placeholder' => $model->getAttributeLabel('name') . '*'
    ]);

    // Телефон
    echo $form->field($model, 'phone', [
        'template' => '{input}<div class="error">{error}</div>',
    ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 (999) 999 99 99',
        'options' => [
            'id' => $formId . '-feedback-phone',
            'autocomplete' => 'off',
            'class' => 'form-control',
            'placeholder' => $model->getAttributeLabel('phone') . '*'
        ],
    ]);

    // E-mail
    echo $form->field($model, 'email', [
        'template' => '{input}<div class="error">{error}</div>',
    ])->textInput([
        'placeholder' => $model->getAttributeLabel('email')
    ]);

    // Примечание
    echo $form->field($model, 'text', [
        'template' => '{input}',
    ])->hiddenInput([
        'value' => $title,
    ]);

    // Название лодки
    echo $form->field($model, 'boat', [
        'template' => '{input}',
    ])->hiddenInput();

    echo Html::submitButton('Записаться', [
        'class' => 'form_callback_button orng_btn',
    ]);

    // </form>
    \app\widgets\ajaxForm\AjaxForm::end();
    ?>
</div>