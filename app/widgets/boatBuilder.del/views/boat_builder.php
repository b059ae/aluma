<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 06.07.2017
 * Time: 21:30
 */
$asset = \app\assets\AppAsset::register($this);
?>
<div class="boat-render-box">
    <img class="render-piece" id="boat-render-image" src="<?= $asset->baseUrl ?>/img/boat/blue-1.jpg?20170913">
</div>

<form class="builder-form">
    <div class="selects allow-one">
        <div class="flex">
            <div class="flex-col-6 selector-decor">
                <?php for ($i = 1; $i <= 5; $i++): ?>
                    <label<?= $i == 1 ? ' class="selected"' : '' ?>>
                        <span class="selector-type">
                            <input name="DecorID" value="<?= $i ?>"
                                   type="radio"<?= $i == 1 ? ' checked="checked"' : '' ?>>
                        </span>
                        <span class="selector-image">
                            <img src="<?= $asset->baseUrl ?>/img/decor/btn-decor-<?= $i ?>-of.png"
                                 width="167"
                                 height="37" border="0">
                        </span>
                    </label>
                <?php endfor; ?>
            </div>
            <div class="flex-col-6 selector-decor">
                <?php foreach (['blue', 'red', 'green'] as $i => $color): ?>
                    <label<?= $i == 0 ? ' class="selected"' : '' ?>>
                        <span class="selector-type">
                            <input name="ColorID" value="<?= $color ?>"
                                   type="radio"<?= $i == 0 ? ' checked="checked"' : '' ?>>
                        </span>
                        <span class="selector-image">
                            <img src="<?= $asset->baseUrl ?>/img/decor/btn-clor-<?= $color ?>-of.png"
                                 width="78"
                                 height="37" border="0">
                        </span>
                    </label>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</form>