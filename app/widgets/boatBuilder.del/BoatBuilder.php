<?php

namespace app\widgets\boatBuilder;

use yii\base\Widget;
use yii\web\View;

/**
 * Выбор узора и цвета
 * Class BoatBuilder
 * @package app\widgets
 */
class BoatBuilder extends Widget
{
    public function run()
    {
        $this->registerJs();
        echo $this->render('boat_builder');
    }

    public function registerJs()
    {
        $script = <<< JS
    $(document).on("change", "div.allow-one label", function() {
        $(this).siblings().removeClass('selected');
        $(this).addClass('selected');
    });
    $(document).on("change", "form.builder-form input", function() {
        save_builder();
    });
   function save_builder() {
        var values = $("form.builder-form").serialize();
        var decor = $('form.builder-form input[name="DecorID"]:checked' ).val();
        var color = $('form.builder-form input[name="ColorID"]:checked' ).val();
        var img = $("#boat-render-image");
        var src = img.prop("src");
        var path = src.substr(0, src.lastIndexOf('/')+1);
        
        img.prop("src",path + color+"-"+decor+".jpg?20170913");
    }
JS;
        $this->view->registerJs($script, View::POS_READY);
    }
}