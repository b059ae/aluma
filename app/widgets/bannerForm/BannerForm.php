<?php
namespace app\widgets\bannerForm;

use app\widgets\bannerForm\models\BannerCardForm;
use yii\base\Widget;
use yii\helpers\Url;

/**
 * Карточка акции
 *
 * @package app\widgets
 */
class BannerForm extends Widget
{
    public $boat; // Название лодки

    public function run()
    {
        echo $this->render('banner_card', [
            'model' => new BannerCardForm(['boat' => $this->boat]), // Форма для заполнения
            'url' => Url::to(['/feedback/banner']), // URL на который будет произведен сабмит
            'metrika' => 'buttonHead', // Цель, которая сработает при сабмите формы
        ]);
    }
}