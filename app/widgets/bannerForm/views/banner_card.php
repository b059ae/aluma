<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 27.05.2017
 * Time: 17:55
 */

/** @var \app\widgets\bannerForm\models\BannerCardForm $model */
/** @var string $url */

/** @var string $metrika */

use yii\helpers\Html;

?>
<div class="form_callback">
    <div class="close_form_btn"></div>
    <p class="form_callback__title">УЗНАЙТЕ АДРЕС ДИЛЕРА <br> В ВАШЕМ ГОРОДЕ</p>
    <p class="form_callback__subtitle">Перезвоним за 8 сек</p>
    <?php
    $formId = uniqid('banner-form', false);

    $form = \app\widgets\ajaxForm\AjaxForm::begin([
        'id' => $formId,
        'metrika' => $metrika,
        'enableClientValidation' => true,
        'action' => $url
    ]);

    // Имя
    echo $form->field($model, 'name', [
        'template' => '{input}<div class="error">{error}</div>',
    ])->textInput([
        'placeholder' => $model->getAttributeLabel('name') . '*'
    ]);

    // Телефон
    echo $form->field($model, 'phone', [
        'template' => '{input}<div class="error">{error}</div>',
    ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 (999) 999 99 99',
        'options' => [
            'id' => $formId . '-feedback-phone',
            'autocomplete' => 'off',
            'class' => 'form-control',
            'placeholder' => $model->getAttributeLabel('phone') . '*'
        ],
    ]);

    // E-mail
    echo $form->field($model, 'email', [
        'template' => '{input}<div class="error">{error}</div>',
    ])->textInput([
        'placeholder' => $model->getAttributeLabel('email')
    ]);

    // Примечание
    echo $form->field($model, 'text', [
        'template' => '{input}',
    ])->hiddenInput([
        'value' => 'Узнайте адрес дилера в Вашем городе',
    ]);

    // Название лодки
    echo $form->field($model, 'boat', [
        'template' => '{input}',
    ])->hiddenInput();

    echo Html::submitButton('узнать', [
        'class' => 'form_callback_button orng_btn',
    ]);

    // </form>
    \app\widgets\ajaxForm\AjaxForm::end();
    ?>
</div>
<div class="form_callback_button-wrapper">
    <p class="text_getknow">УЗНАЙТЕ АДРЕС ДИЛЕРА <br>В ВАШЕМ ГОРОДЕ</p>
    <button class="form_callback_button orng_btn" id="getknow">узнать</button>
</div>
<?php
// Отображение формы по клику
$script = <<< JS
    $("#getknow").click(function(){
        $(".form_callback").show().animateCss('bounceInLeft'); // show
        $("#getknow").animateCss('fadeOutDown', function () {
           $("#getknow").hide();
        }); // hide
    });
    $(".close_form_btn").click(function(){
        $(".form_callback").animateCss('bounceOutLeft', function () {
           $(".form_callback").hide();
        }); // hide
        $("#getknow").show().animateCss('fadeInUp');
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
