<?php

namespace app\widgets\bannerForm\actions;

use app\widgets\bannerForm\models\BannerCardForm;
use Yii;
use yii\base\Action;

class BannerCardAction extends Action
{
    /**
     * Форма Доставка в подарок
     * @return string
     */
    public function run()
    {
        $model = new BannerCardForm();

        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->save();
        }
        return ;
    }
}