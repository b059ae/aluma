<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:50
 */

namespace app\widgets\callback;

use app\widgets\callback\assets\CallAsset;

use app\widgets\callback\models\CallbackForm;
use yii\base\Widget;
use yii\helpers\Url;

/**
 * Плавающая кнопка обратного звонка
 * Class CallbackFloatButton
 * @package app\widgets\callback
 */
class CallbackFloatButton extends Widget
{
    public $buttonTitle = 'Заказать звонок';

    public function run()
    {
        $this->registerAssetBundle(); // Стили для плавающей кнопки
        echo $this->render('callback_float_button', [
            'model' => new CallbackForm(),// Форма для заполнения
            'url' => Url::to(['/feedback/callback']), // URL на который будет произведен сабмит
            'metrika' => 'callback', // Цель, которая сработает при сабмите формы
            'title' => 'Заказать бесплатный звонок',
            'buttonTitle' => $this->buttonTitle,
        ]);
    }

    private function registerAssetBundle()
    {
        CallAsset::register($this->getView());
    }
}