<?php
namespace app\widgets\callback\assets;


class CallAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/widgets/callback';
    public $css = [
        'css/call.css',
    ];
    public $js = [
    ];
    public $depends = [
        \yii\easyii\assets\FancyboxAsset::class,
    ];
}
