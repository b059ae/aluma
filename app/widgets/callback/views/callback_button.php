<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:53
 */

/** @var $url string */
/** @var $model \app\widgets\callback\models\CallbackForm */
/** @var $metrika string */
/** @var $title string */
/** @var $buttonCssCClass string */
/** @var $buttonTitle string */
/** @var $boat string */

$popupId = uniqid('callback', false);
?>
<!--Модальное окно-->
<?= $this->render('popup', [
    'popupId' => $popupId,
    'url' => $url,
    'model' => $model,
    'metrika' => $metrika,
    'title' => $title,
    'buttonTitle' => $buttonTitle,
    'boat' => $boat,
]) ?>
<!--/Модальное окно-->
<!--Кнопка-->
<a class="<?=$buttonCssClass?> fancybox" href="#<?= $popupId ?>"><?=$buttonTitle?></a>
<!--/Кнопка-->
