<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:53
 */

/** @var $url string */
/** @var $model \app\widgets\callback\models\CallbackForm */
/** @var $metrika string */
/** @var $title string */
/** @var $buttonTitle string */

$popupId = uniqid('callback_float', false);
?>
<!--Модальное окно-->
<?= $this->render('popup', [
    'popupId' => $popupId,
    'url' => $url,
    'model' => $model,
    'metrika' => $metrika,
    'title' => $title,
    'buttonTitle' => $buttonTitle,
    'boat' => '',
]) ?>
<!--/Модальное окно-->
<!--Плавающая кнопка-->
<jdiv>
    <jdiv class="fancybox" href="#<?= $popupId ?>">
        <jdiv class="globalClass_ET">
            <jdiv style="background-color: rgb(68, 187, 110);"
                  class="wrap_3s _show_3w">
                <jdiv style="color: rgb(68, 187, 110);"
                      class="button_3b"></jdiv>
                <jdiv class="iconWrap_1u">
                    <jdiv iconname="callback-btn" class="logoCallback_w0">
                        <svg width="33px" height="33px" viewBox="0 0 33 33"
                             version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink">
                            <defs>
                                <path
                                        d="M24.2,33.4333333 C26.6111111,38.2555556 30.7444444,42.2166667 35.5666667,44.8 L39.3555556,41.0111111 C39.8722222,40.4944444 40.5611111,40.3222222 41.0777778,40.6666667 C42.9722222,41.3555556 45.0388889,41.7 47.2777778,41.7 C48.3111111,41.7 49,42.3888889 49,43.4222222 L49,49.2777778 C49,50.3111111 48.3111111,51 47.2777778,51 C31.0888889,51 18,37.9111111 18,21.7222222 C18,20.6888889 18.6888889,20 19.7222222,20 L25.75,20 C26.7833333,20 27.4722222,20.6888889 27.4722222,21.7222222 C27.4722222,23.7888889 27.8166667,25.8555556 28.5055556,27.9222222 C28.6777778,28.4388889 28.5055556,29.1277778 28.1611111,29.6444444 L24.2,33.4333333 Z"
                                        id="path-1"></path>
                                <filter x="-50%" y="-50%" width="200%"
                                        height="200%"
                                        filterUnits="objectBoundingBox">
                                    <feOffset dx="0" dy="1" in="SourceAlpha"
                                              result="shadowOffsetOuter1"></feOffset>
                                    <feGaussianBlur stdDeviation="0.5"
                                                    in="shadowOffsetOuter1"
                                                    result="shadowBlurOuter1"></feGaussianBlur>
                                    <feColorMatrix
                                            values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.0434782609 0"
                                            type="matrix"
                                            in="shadowBlurOuter1"></feColorMatrix>
                                </filter>
                            </defs>
                            <g stroke="none" stroke-width="1" fill="none"
                               fill-rule="evenodd"
                               transform="translate(-116.000000, -976.000000)">
                                <g transform="translate(99.000000, 956.000000)">
                                    <use fill="#FFFFFF" fill-rule="evenodd"
                                         xlink:href="#path-1"></use>
                                </g>
                            </g>
                        </svg>
                    </jdiv>
                </jdiv>
            </jdiv>
        </jdiv>
    </jdiv>
</jdiv>
<!--/Плавающая кнопка-->
