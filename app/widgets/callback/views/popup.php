<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 03.06.2017
 * Time: 16:55
 */

use app\models\Region;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var $popupId string */
/** @var $url string */
/** @var $model \app\widgets\callback\models\CallbackForm */
/** @var $metrika string */
/** @var $title string */
/** @var $buttonTitle string */
/** @var $boat string */

?>
<div style="display:none">
    <div id="<?= $popupId ?>">
        <div class="tricard-single formContainer">
            <div class="banner-card-header">
                <?=$title?>
            </div>
            <div class="flex banner-card-info center">
                <div class="flex-col-12">
                    <div class="tricard-icon-wrap">
                        <p class="boat-styles">
                            Перезвоним за 8 секунд
                        </p>
                    </div>
                    <div>
                        <?php
                        $formId = uniqid('callback-popup', false);

                        $form = \app\widgets\ajaxForm\AjaxForm::begin([
                            'id' => $formId,
                            'metrika' => $metrika,
                            'enableClientValidation' => true,
                            'action' => $url,
                            'fieldConfig' => [
                                'options' => [
                                    'class' => 'flex-col-12',
                                ],
                            ],
                        ]);
                        ?>
                        <div class="flex form-group">
                            <?php
                            // Имя
                            echo $form->field($model, 'name', [
                                'template' => '<span class="error">*</span>{input}<div class="error">{error}</div>',
                            ])->textInput([
                                'placeholder' => $model->getAttributeLabel('name')
                            ]);

                            // Телефон
                            echo $form->field($model, 'phone', [
                                'template' => '<span class="error">*</span>{input}<div class="error">{error}</div>',
                            ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '+7 (999) 999 99 99',
                                'options' => [
                                    'id' => $formId . '-feedback-phone',
                                    'autocomplete' => 'off',
                                    'class' => 'form-control',
                                    'placeholder' => $model->getAttributeLabel('phone')
                                ],
                            ]);

                            // Примечание
                            echo $form->field($model, 'text',[
                                'template' => '{input}',
                            ])->hiddenInput([
                                'value' => $title,
                            ]);

                            // Название лодки
                            echo $form->field($model, 'boat', [
                                'template' => '{input}',
                            ])->hiddenInput([
                                'value' => $boat,
                            ]);
                            ?>
                        </div>

                        <div class="flex form-group">
                            <div class='flex-col-12'>
                                <?= Html::submitInput($buttonTitle, [
                                    'class' => 'orangeBtn',
                                ]); ?>
                            </div>
                        </div>
                        <?php \app\widgets\ajaxForm\AjaxForm::end(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>