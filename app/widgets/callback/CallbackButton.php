<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:50
 */

namespace app\widgets\callback;

use app\widgets\callback\assets\CallAsset;

use app\widgets\callback\models\CallbackForm;
use yii\base\Widget;
use yii\helpers\Url;

/**
 * Плавающая кнопка обратного звонка
 * Class CallbackButton
 * @package app\widgets\callback
 */
class CallbackButton extends Widget
{
    public $title = 'Заказать бесплатный звонок';
    public $buttonTitle = 'Заказать звонок';
    public $buttonCssClass = '';
    public $boat = '';

    public function run()
    {
        echo $this->render('callback_button', [
            'model' => new CallbackForm(),// Форма для заполнения
            'url' => Url::to(['/feedback/callback']), // URL на который будет произведен сабмит
            'metrika' => 'callback', // Цель, которая сработает при сабмите формы,
            'title' => $this->title,// Заголовок окна
            'buttonCssClass' => $this->buttonCssClass,// Класс кнопки
            'buttonTitle' => $this->buttonTitle,// Текст на кнопке
            'boat' => $this->boat,// Название лодки
        ]);
    }
}