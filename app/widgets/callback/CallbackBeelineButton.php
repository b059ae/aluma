<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 14:50
 */

namespace app\widgets\callback;

use yii\base\Widget;

/**
 * Плавающая кнопка для Билайна
 * Class CallbackBeelineButton
 * @package app\widgets\callback
 */
class CallbackBeelineButton extends Widget
{

    public function run(){
        echo '<script type="text/javascript" src="https://vn.beeline.ru/com.broadsoft.xsi-actions/test/v2.0/user/userid/calls/callmenow/mpbx/mpbx-cmn-frame.js?user=MPBX_g_76328_ivr_79723%40mpbx.sip.beeline.ru&theme=1&color=4&opened=1"></script>';
    }
}