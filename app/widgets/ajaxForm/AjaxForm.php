<?php
namespace app\widgets\ajaxForm;

use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * Форма с отправкой данных аяксом
 * Class AbstractAjaxForm
 * @package app\widgets
 */
class AjaxForm extends ActiveForm
{
    /**
     * Цель метрики, которая сработает при сабмите формы
     * @var string
     */
    public $metrika = '';

    private $afterSubmitModalId;

    public function init()
    {
        parent::init();
        $this->afterSubmitModalId = 'after_submit_modal_'.$this->id;
    }

    public function afterRun($result){
        // JS, заменяющий сабмит формы
        $this->registerJs();
        // Окно с сообщением после сабмита
        return $result.$this->afterSubmitModal();
    }

    /**
     * Регистрация аякс-обработчика
     */
    private function registerJs()
    {
        $m = !empty($this->metrika)
            ? 'metrikaReachGoal(\'' . $this->metrika . '\');'
            : '';
        $script = <<< JS
                    $('body').on('beforeSubmit', 'form#{$this->id}', function () {
                         var form = $(this);
                         // return false if form still have some validation errors
                         if (form.find('.has-error').length) {
                              return false;
                         }
                         // submit form
                         $.ajax({
                              url: form.attr('action'),
                              type: 'post',
                              data: form.serialize(),
                              beforeSend: function () {
                                   //hide all modals
                                   $.fancybox.close(true);
                                   //open thank you modal
                                   setTimeout(function(){
                                        $.fancybox.open({href: '#{$this->afterSubmitModalId}'},{
                                            type: 'inline',
                                            minWidth: 320,
                                            maxWidth: 957,
                                            maxHeight: 1000,
                                            fitToView: true,
                                            width: '100%',
                                            height: '100%',
                                            closeClick: false,
                                            scrolling: 'auto',
                                            padding: 0,
                                            helpers: {
                                                overlay: {
                                                    locked: true
                                                }
                                            }
                                       });
                                    }, 300);
                                    {$m}

                                   //remove values in fields
                                   form.find('input[type="text"]').val('');
                              }
                         });
                         return false;
                    });
JS;
        $this->view->registerJs($script, View::POS_READY);
    }

    /**
     * Модальное окно с благодарностью
     * @return string
     */
    private function afterSubmitModal()
    {
        return $this->render('after_submit_modal', [
            'id' => $this->afterSubmitModalId,
        ]);
    }
}