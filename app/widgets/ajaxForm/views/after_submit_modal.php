<?php
/** @var $this \yii\web\View */
/** @var $id string */

?>
<div style="display:none">
    <div id="<?= $id ?>">
        <div class="tricard-single formContainer">
            <div class="banner-card-header">
                Спасибо за Вашу заявку!
            </div>
            <div class="flex banner-card-info center">
                <div class="flex-col-12">
                    <p>Наши специалисты оперативно свяжутся и проконсультируют Вас по видам и характеристикам продукции.</p>
                </div>
            </div>
        </div>
    </div>
</div>