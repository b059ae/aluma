<?php

namespace app\widgets\consultationForm\models;

use app\models\AbstractFeedbackForm;
use yii\easyii\modules\feedback\api\Feedback;
use yii\helpers\ArrayHelper;

class ConsultationForm extends AbstractFeedbackForm
{
    public $region;

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'region' => 'Регион',
        ]);
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            ['region', 'string', 'max' => 128],
        ]);
    }

    public function save()
    {
        $model = new Feedback();
        $model->api_save([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'text' => 'Получить консультацию. Регион ' . $this->region,
        ]);
    }
}