<?php
namespace app\widgets\consultationForm;

use yii\base\Widget;
use yii\helpers\Url;

/**
 * Форма Получить консультацию
 * Class DeliveryForm
 * @package app\widgets
 */
class ConsultationForm extends Widget
{

    public function run()
    {
        echo $this->render('consultation', [
            'model' => new \app\widgets\consultationForm\models\ConsultationForm(), // Форма для заполнения
            'url' => Url::to(['/feedback/consultation']), // URL на который будет произведен сабмит
            'metrika' => 'buttonConsulting', // Цель, которая сработает при сабмите формы
        ]);
    }
}