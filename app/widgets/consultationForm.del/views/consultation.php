<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 27.05.2017
 * Time: 17:55
 */

/** @var \app\widgets\bannerForm\models\BannerCardForm $model */
/** @var string $url */
/** @var string $metrika */

use app\widgets\consultationForm\models\Region;
use yii\helpers\Html;

$title = 'Получить консультацию ';
?>

<div class="tricard-single formContainer">
    <div class="banner-card-header">
        <?=$title?>
    </div>
    <div class="banner-card-info center flex-col-12">
        <div class="tricard-icon-wrap">
            <p class="boat-styles">
                Перезвоним за 8 секунд
            </p>
        </div>
        <?php
        $formId = uniqid('banner-form', false);

        $form = \app\widgets\ajaxForm\AjaxForm::begin([
            'id' => $formId,
            'metrika' => $metrika,
            'enableClientValidation' => true,
            'action' => $url,
            'fieldConfig' => [
                'options' => [
                    'class' => 'flex-col-12',
                ],
            ],
        ]);
        ?>
        <div class="flex form-group">
            <?php
            // Имя
                echo $form->field($model, 'name', [
                    'template' => '<span class="error">*</span>{input}<div class="error">{error}</div>',
                ])->textInput([
                    'placeholder' => $model->getAttributeLabel('name')
                ]);

                // Телефон
                echo $form->field($model, 'phone', [
                    'template' => '<span class="error">*</span>{input}<div class="error">{error}</div>',
                ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999) 999 99 99',
                    'options' => [
                        'id' => $formId . '-feedback-phone',
                        'autocomplete' => 'off',
                        'class' => 'form-control',
                        'placeholder' => $model->getAttributeLabel('phone')
                    ],
                ]);

                // E-mail
                echo $form->field($model, 'email', [
                    'template' => '{input}<div class="error">{error}</div>',
                ])->textInput([
                    'placeholder' => $model->getAttributeLabel('email')
                ]);

                // Регион
                echo $form->field($model, 'region', [
                    'template' => '{input}<div class="error">{error}</div>',
                ])->dropDownList(Region::all(), [
                    'prompt' => $model->getAttributeLabel('region')
                ]);
            ?>
        </div>

        <div class="flex form-group">
            <div class='flex-col-12'>
                <?= Html::submitInput('Отправить заявку', [
                    'class' => 'orangeBtn',
                ]); ?>
            </div>
        </div>
        <?php \app\widgets\ajaxForm\AjaxForm::end(); ?>

    </div>
</div>