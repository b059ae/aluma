<?php

namespace app\widgets\consultationForm\actions;

use app\widgets\consultationForm\models\ConsultationForm;
use Yii;
use yii\base\Action;

class ConsultationAction extends Action
{
    /**
     * Форма Доставка в подарок
     * @return string
     */
    public function run()
    {
        $model = new ConsultationForm();

        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->save();
        }
        return ;
    }
}