<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 03.06.2017
 * Time: 15:24
 */

namespace app\api;
use yii\easyii\modules\entity\api\Entity as EntityApi;
use yii\easyii\modules\entity\models\Category;


class EntitySlugItems extends EntityApi
{

    private $_cats_app;

    /**
     * Получение категории по slug, приведение к массиву с ключами равными slug
     * @param string $id_slug
     * @return mixed
     */
    public function api_cat($id_slug)
    {
        if(!isset($this->_cats_app[$id_slug])) {
            $this->_cats_app[$id_slug] = new CategoryObject(Category::get($id_slug));
        }
        return $this->_cats_app[$id_slug];
    }
}