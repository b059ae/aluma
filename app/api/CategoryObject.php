<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 05.06.17
 * Time: 11:25
 */

namespace app\api;

use yii\data\ActiveDataProvider;
use yii\easyii\helpers\Data;
use yii\easyii\modules\entity\api\ItemObject;
use yii\easyii\modules\entity\models\Category;
use yii\easyii\modules\entity\models\Item;


class CategoryObject extends \yii\easyii\modules\entity\api\CategoryObject
{
    private $_adp_app;

    public function getItems($options = [])
    {
        $result = [];

        if($this->cache)
        {
            $result = Data::cache(Category::getCacheName($this->id), 3600, function(){
                $items = [];
                $query = Item::find()->where(['category_id' => $this->id])->status(Item::STATUS_ON)->sort();
                foreach($query->all() as $item){
                    $object = new ItemObject($item);
                    $items[$object->slug] = $object;
                }
                return $items;
            });
        }
        else
        {
            $query = Item::find()->where(['category_id' => $this->id])->status(Item::STATUS_ON);

            if(!empty($options['where'])){
                $query->andFilterWhere($options['where']);
            }
            $query->sort();

            $this->_adp_app = new ActiveDataProvider([
                'query' => $query,
                'pagination' => !empty($options['pagination']) ? $options['pagination'] : []
            ]);

            foreach($this->_adp_app->models as $model){
                $object = new ItemObject($model);
                $result[$object->slug] = $object;
            }
        }
        return $result;
    }
}