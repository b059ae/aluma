<?php
namespace app\assets;

class SlicknavAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/slicknav/dist';

    public $css = [
        //'slicknav.css',
    ];
    public $js = [
        'jquery.slicknav.min.js'
    ];

    public $depends = ['yii\web\JqueryAsset'];
}