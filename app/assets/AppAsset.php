<?php
namespace app\assets;


class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/slicknav.css',// Custom SlickNav
        'css/aluma-custom.css?20171227',
        'css/styles.css?20171219',
        'css/aluma_fish.css?20180313',
    ];
    public $js = [
        'js/scripts.js?20171219',
        'js/jquery.malihu.PageScroll2id.js?20171125',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        SlicknavAsset::class,
        'rmrevin\yii\fontawesome\AssetBundle',
        \yii\easyii\assets\FancyboxAsset::class,
        \yii\easyii\assets\AnimateAsset::class,
        \yii\easyii\assets\WowAsset::class,
//        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
