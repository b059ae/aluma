<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 11.12.2017
 * Time: 21:33
 */
/** @var $news \yii\easyii\modules\article\api\CategoryObject[] */
?>
<section class="latest-news pad-40">
    <div class="container modifications__container">
        <h1 class="modifications__title wow fadeInLeft">Новости</h1>
        <div class="news-posts-wrap flex">

            <?php foreach ($news as $item): ?>
                <div class="news-cards flex-col-4 wow fadeInUp">
                    <div class="news-cards-single flex advantages__container">
                        <div class="news-cards-image flex-col-12"
                             style="background-image: url('<?= $item->thumb(424, 200) ?>');">
                        </div>
                        <div class="news-cards-info flex-col-12">
                            <a href="<?= \yii\helpers\Url::to(['/news/view', 'slug' => $item->slug]) ?>">
                                <h3><?= $item->title ?></h3>
                                <p class="post-snippet"><?= $item->short ?></p>
                            </a>
                            <?php if (strlen($item->source) > 0): ?>
                                <p class="post-source">
                                    Источник: <?=
                                    \yii\helpers\Html::a(
                                        strlen($item->source) > 40
                                            ? $item->getSource(40) . '…'
                                            : $item->source,
                                        $item->source, [
                                        "rel" => "nofollow",
                                        "target" => "_blank",
                                    ])
                                    ?>
                                </p>
                            <?php endif; ?>
                            <p class="post-by"><?= $item->date ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
