<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 20.07.2017
 * Time: 23:19
 */
/** @var $article \yii\easyii\modules\article\api\ArticleObject */
$this->registerMetaTag([
    'name' => 'description',
    'content' => $article->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $article->seo('keywords')
]);
$this->title = $article->seo('title', $article->title);
?>
<section class="grey news-blog pad-100">
    <div class="container modifications__container">
        <h1 class="modifications__title"><?= $article->title ?></h1>
        <div class="wrap small-wrap flex flex-with-gutter">
            <div class="flex-col-12 right">
                <div class="addthis_inline_share_toolbox"></div>
            </div>
            <div class="flex-col-2">
                <p class="post-by"><?= $article->date ?></p>
            </div>

            <div class="flex-col-10">
                <?= $article->text ?>
                <?php if (strlen($article->source) > 0): ?>
                    <p>
                        Источник: <?=
                        \yii\helpers\Html::a(
                            strlen($article->source) > 80
                                ? $article->getSource(80) . '…'
                                : $article->source,
                            $article->source, [
                            "rel" => "nofollow",
                            "target" => "_blank",
                        ])
                        ?>
                    </p>
                <?php endif; ?>
                <div class="buttn-wrap">
                    <a href="<?= \yii\helpers\Url::to(['/news']) ?>" class="button-primary">Список новостей</a>
                </div>
            </div>
        </div>
    </div>

</section>