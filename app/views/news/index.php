<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 20.07.2017
 * Time: 23:19
 */
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
/** @var $news \yii\easyii\modules\article\api\CategoryObject[] */
$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);
$this->title = $cat->seo('title', $cat->title);
?>
<div class="pad-60">
    <?= $this->render('//news/_index', ['news' => $news]); ?>
</div>