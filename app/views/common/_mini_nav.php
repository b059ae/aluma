<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 27.05.2017
 * Time: 16:52
 */
?>
<section class="cta-mini-nav">
    <div class="wrap flex">
        <div class="flex-col-6 mini-nav-blocks center">
            <a href="#stoimost">
                <i class="fa fa-rub" aria-hidden="true"></i>
                <h3>Узнать стоимость</h3>
            </a>
        </div>
        <div class="flex-col-6 mini-nav-blocks center">
            <?=\app\widgets\gift\GiftForm::widget(); ?>
        </div>
        <?php /*
        <div class="flex-col-4 mini-nav-blocks center">
            <a class="fancybox" href="#brochure">
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                <h3>Получить брошюру</h3>
            </a>
            <?=\app\widgets\popup\BrochurePopup::widget(['popupId'=>'brochure']); ?>
        </div>
        */ ?>
    </div>
</section>