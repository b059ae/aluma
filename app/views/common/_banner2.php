<?php
/** @var $asset yii\web\AssetBundle */
?>
<section class="main-banner parallax">
    <div class="banner-main">
        <div class="banner-left">
            <div class="main_shadow">
            </div>
            <div class="main_logo">
                <img src="<?= $asset->baseUrl ?>/img/fishlogo3d.png" />
            </div>
            <div class="main_text">
                <p>Для рыбалки и отдыха</p>
                <img src="<?= $asset->baseUrl ?>/img/fishlogo.png" />
                <button>Подробнее</button>
            </div>
        </div>
        <a href="/dream-6500">
            <div class="banner-right">
                <div class="main_shadow">
                </div>
                <div class="main_logo">
                    <img src="<?= $asset->baseUrl ?>/img/dreamlogo3d.png" />
                </div>
                <div class="main_text">
                    <p>Для отдыха</p>
                    <img src="<?= $asset->baseUrl ?>/img/dreamlogo.png" />
                    <button>Подробнее</button>
                </div>
            </div>
        </a>
    </div>
    <div class="banner-fish">
        <div class="boats">
            <div class="fish_boat">
                <a href="/fish-47">
                    <img class="fish-landscape" src="<?= $asset->baseUrl ?>/img/slide-fish/fish4.7.png"/>
                    <img class="fish-mobile" src="<?= $asset->baseUrl ?>/img/slide-fish/fish4.7-h.png?20121222"/>
                </a>
            </div>
            <div class="fish_boat">
                <a href="/fish-51">
                    <img class="fish-landscape" src="<?= $asset->baseUrl ?>/img/slide-fish/fish5.1.png"/>
                    <img class="fish-mobile" src="<?= $asset->baseUrl ?>/img/slide-fish/fish5.1-h.png?20121222"/>
                </a>
            </div>
            <div class="fish_boat">
                <a href="/fish-55">
                    <img class="fish-landscape" src="<?= $asset->baseUrl ?>/img/slide-fish/fish5.5.png"/>
                    <img class="fish-mobile" src="<?= $asset->baseUrl ?>/img/slide-fish/fish5.5-h.png?20121222"/>
                </a>
            </div>
        </div>
    </div>
</section>