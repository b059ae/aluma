<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 27.05.2017
 * Time: 16:33
 */

/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */

use yii\easyii\helpers\Upload;

/** @var $h1 string */
/** @var $youtubeId string */
/** @var $photo null|string */
$style = '';
if ($photo) {
    $style = 'style="background-image: url(\'' . $photo . '\')"';
}
?>
<section class="hero-banner parallax <?= $boat->slug ?>" <?= $style ?>>
    <!--<video loop autoplay muted playsinline class="bg-video">
        <source type="video/webm" src="/images/home/cta-content/live-for-the-water.webm">
        <source type="video/mp4" src="/images/home/cta-content/live-for-the-water.mp4">
    </video>-->

    <div class="banner-wrap">
        <div class="hero-info-box">
            <h1 class="wow fadeInLeft"><?= $h1 ?></h1>
            <?php /*<h3>Доставка в любой регион РФ и стран СНГ</h3> */ ?>
            <?php /*<p>You’re invited to join Crestliner’s Launch Into Summer Hometown Tour. Dealers across North America
                are opening their doors to host one-of-a-kind events with prizes, giveaways and more. </p>*/ ?>
            <div class="hero-button-wrap">
                <?= \app\widgets\bannerButton\BannerButton::widget(); ?>
                <a class="button-secondary fancybox-video wow fadeInLeft"
                   onclick="metrikaReachGoal('buttonVideo'); return true;"
                   href="https://www.youtube.com/embed/<?= $youtubeId ?>?autoplay=1&amp;rel=0&amp;controls=1&amp;showinfo=0">
                    <i class="fa fa-play" aria-hidden="true"></i> Посмотреть видео
                </a>
            </div>
        </div>
        <?= \app\widgets\bannerForm\BannerForm::widget(['boat' => $boat->getTitle()]); ?>
    </div>
</section>