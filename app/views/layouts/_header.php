<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */

use app\helpers\Phone;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Url;
use yii\widgets\Menu;

?>
<div class="header sneak">
    <!-- Первый хедер -->
    <div class="header-top">
        <div class="wrap left">
            <ul>
                <li>
                    <a href="tel:<?= Phone::link(Text::get('phone')) ?>">
                        <i class="fa fa-mobile"></i> <?= Text::get('phone') ?>
                        <span class="hidden-xs"><small>Звонок бесплатный</small></span>
                        <span class="hidden-xs"><small>Бесплатно</small></span>
                    </a>
                </li>
                <li>
                    <a href="mailto:<?= Text::get('mail') ?>"><i class="fa fa-envelope-o"></i> <?= Text::get('mail') ?></a>
                </li>
                <li>
                    <a href="/">
                        Ru
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- / Первый хедер -->

    <!-- Второй хедер -->
    <div class="header-bottom">
        <div class="wrap">
            <div class="logo"><a href="/">Aluma</a>
            </div>
            <div class="build-mobile-wrapper">
                <div class="buttn-wrap">
                    <!-- Кнопка обратного звонка-->
                    <?= \app\widgets\callback\CallbackButton::widget([
                            'title' => 'Заказать бесплатный звонок',
                            'buttonTitle' => 'Заказать звонок',
                            'buttonCssClass'=>'button-secondary',
                    ]); ?>
                </div>
<!--                <?php /*foreach (yii\easyii\modules\menu\api\Menu::items('extra') as $item): */?>
                    <a class="build-price-button" href="<?/*= $item['url'] */?>"><?/*= $item['label'] */?></a>
                --><?php /*endforeach; */?>
                <div class="mobile-nav-wrap" style="display: none;">
                    <ul id="mobile-nav" style="display: none;">
                        <li>
                            <a class="slicknav_row"
                               href="/">Главная</a>
                        </li>
                        <li class="dropbtn">Каталог
                            <ul>
                                <li>
                                    <img class="menu-logo" src="<?= $asset->baseUrl ?>/img/menu/aluma_fish.jpg" />
                                    <a href="/fish-47"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_fish4.7.jpg" /></a>
                                    <a href="/fish-51"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_fish5.1.jpg" /></a>
                                    <a href="/fish-55"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_fish5.5.jpg" /></a>
                                    <img class="menu-logo" src="<?= $asset->baseUrl ?>/img/menu/aluma_dream.png"/>
                                    <a href="/dream-6500"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_dream6500.png" class="dream"/></a>
                                </li>
                            </ul>
                        </li>
                        <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                            <li>
                                <a class="slicknav_row"
                                   href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!--    mobile-nav -->
            </div>
            <ul id="main-navigation">
                <li>
                    <a class="slicknav_row"
                       href="/">Главная</a>
                </li>
                <button id="dropbtn" class="dropbtn" onclick="dropdownCatalog()">Каталог
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content" id="drop_catalog">
                    <img class="menu-logo" src="<?= $asset->baseUrl ?>/img/menu/aluma_fish.jpg" />
                    <a href="/fish-47"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_fish4.7.jpg" /></a>
                    <a href="/fish-51"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_fish5.1.jpg" /></a>
                    <a href="/fish-55"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_fish5.5.jpg" /></a>
                    <img class="menu-logo" src="<?= $asset->baseUrl ?>/img/menu/aluma_dream.png"/>
                    <a href="/dream-6500"><img src="<?= $asset->baseUrl ?>/img/menu/aluma_dream6500.png" class="dream" /></a>
                </div>
                <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                    <li>
                        <a class="nav-link" href="<?= $item['url'] ?>"><?= $item['label'] ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <!-- / Второй хедер -->
</div>