<?php
/** @var $this \yii\web\View */

/** @var $asset \yii\web\AssetBundle */

use app\helpers\Html;
use app\helpers\Phone;
use yii\easyii\models\Setting;
use yii\easyii\modules\text\api\Text;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Menu;

?>
<footer class="footerContainer">
    <div class="wrap flex flex-justify-space-between">
        <div class="flex-col-2">
            <img src="<?= $asset->baseUrl ?>/img/logo.png"/>
            <p class="dealer-info">
                    <span class="city_dealer-info"><?= Text::get('address') ?><br>
                    <span class="city_dealer-blue">Почта: </span> <a
                                href="mailto:<?= Text::get('mail') ?>"><?= Text::get('mail') ?></a><br>
                    <span class="city_dealer-blue">Телефон: </span> <a
                                href="tel:<?= Phone::link(Text::get('phone')) ?>"><?= Text::get('phone') ?></a>
            </p>
            <ul class="footer-social-icons">
                <li>
                    <a href="<?= Text::get('facebook') ?>" target="_blank">
                        <i class="fa fa-facebook" aria-hidden="true"></i>

                    </a>
                </li>
                <li>
                    <a href="<?= Text::get('vkontakte') ?>" target="_blank">
                        <i class="fa fa-vk" aria-hidden="true"></i>
                    </a>
                </li>
                <li>
                    <a class="" href="<?= Text::get('instagram') ?>"
                       target="_blank">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                </li>
                <li>
                    <a href="<?= Text::get('youtube') ?>"
                       target="_blank">
                        <i class="fa fa-youtube" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="flex-col-6 footer_menu">
            <a href="/" class="footer_menu_link">каталог</a>
            <?php foreach (yii\easyii\modules\menu\api\Menu::items('main') as $item): ?>
                <a href="<?= $item['url'] ?>" class="footer_menu_link"><?= $item['label'] ?></a>
            <?php endforeach; ?>
            <p class="copyright"><b>ALUMA</b> ©COPYRIGHT <?=date('Y')?>. </p>
        </div>
        <div class="flex-col-3 dealer-info">
            <div class="buttn-wrap">
                <!-- Кнопка обратного звонка-->
                <?= \app\widgets\callback\CallbackButton::widget([
                        'title' => 'Заказать бесплатный звонок',
                        'buttonTitle' => 'Заказать бесплатный звонок',
                        'buttonCssClass'=>'button-secondary',
                ]); ?>
            </div>
            <p>Звонок бесплатный по всей территории РФ</p>
            <h3><a href="tel:<?= Phone::link(Text::get('phone')) ?>"><?= Text::get('phone') ?></a></h3>
        </div>
    </div>
</footer>
<?php /** Плавающая кнопка обратного звонка */ ?>
<?= \app\widgets\callback\CallbackFloatButton::widget() ?>

<?php /** Плавающая кнопка обратного звонка через Билайн */ ?>
<?php /* =\app\widgets\callback\CallbackBeelineButton::widget()*/ ?>

<?php /** Виджет онлайн-чата */ ?>
<?= \app\widgets\Chat::widget() ?>
