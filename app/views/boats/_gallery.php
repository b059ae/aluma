<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 19:06
 */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
?>
<section class="gallery">
    <h2 class = "gallery-title wow fadeInLeft">Галерея</h2>
    <div class="container slider_gallery">
<!--        <div class="slider_element mySlides fade">-->
        <div class="slider_element fade wow fadeInUp">
            <div class="date_gallery">
<!--                <p class="date-gallery-text">01 <span class="month">/ 16</span></p>-->
            </div>
            <div class="wrap related-media-wrapper flex">
                <?php foreach (array_slice($boat->getPhotos(), 0, 12) as $photo): ?>
                    <div class="media-block wow fadeIn">
                        <a class="fancyboxAuto" rel="related-media" href="<?= $photo->getImage() ?>">
                            <img src="<?= $photo->thumb(425, 135) ?>">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
<!--        <div class="slider_element mySlides fade">-->
<!--            <div class="date_gallery">-->
<!--                <p class="date-gallery-text">01 <span class="month">/ 17</span></p>-->
<!--            </div>-->
<!--            <div class="wrap related-media-wrapper flex">-->
<!---->
<!---->
<!--                <div class="media-block wow fadeIn" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="http://test.aluma-boats.ru/uploads/catalog/14-616a401cfd.jpg">-->
<!--                        <img src="img/14-616a401cfd-0f1ad0702fa30a7df0148b897b9e16c2.jpg">-->
<!--                    </a>-->
<!--                </div>-->
<!---->
<!--                <div class="media-block wow fadeIn" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="http://test.aluma-boats.ru/uploads/catalog/20-ce16b1a65f.jpg">-->
<!--                        <img src="img/20-ce16b1a65f-0f1ad0702fa30a7df0148b897b9e16c2.jpg">-->
<!--                    </a>-->
<!--                </div>-->
<!--                <div class="media-block wow fadeIn" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="http://test.aluma-boats.ru/uploads/catalog/7-3773d68d68.jpg">-->
<!--                        <img src="img/7-3773d68d68-0f1ad0702fa30a7df0148b897b9e16c2.jpg">-->
<!--                    </a>-->
<!--                </div>-->
<!--                <div class="media-block wow fadeIn" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="http://test.aluma-boats.ru/uploads/catalog/13-6a90849045.jpg">-->
<!--                        <img src="img/13-6a90849045-0f1ad0702fa30a7df0148b897b9e16c2.jpg">-->
<!--                    </a>-->
<!--                </div>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="container container_next"><a class="next" onclick="plusSlides(1)">далее</a></div>-->
    </div>

</section>
<?php if (strlen($boat->video) > 0): ?>
<section class="gallery gallery_videos_slider">
    <div class="container slider_gallery">
<!--        <div class="slider_element mySlides_video fade video_sl">-->
        <div class="slider_element fade video_sl wow fadeInUp">
            <div class="date_gallery">
<!--                <p class="date-gallery-text">01 <span class="month">/ 16</span></p>-->
            </div>
            <div class="wrap related-media-wrapper flex">
<!--                <div class="media-block  wow fadeIn video_gallery_block" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="img/video1.png">-->
<!--                        <img src="img/video1.png">-->
<!--                    </a>-->
<!--                </div>-->
<!--                <div class="media-block wow fadeIn video_gallery_block" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="img/video2.png">-->
<!--                        <img src="img/video2.png">-->
<!--                    </a>-->
<!--                </div>-->
                <?php if (strlen($boat->video) > 0): ?>
                    <iframe class="wow fadeInUp" width="100%" height="550"
                            src="https://www.youtube.com/embed/<?= $boat->video ?>?rel=0&showinfo=0" frameborder="0"
                            allowfullscreen></iframe>
                <?php endif; ?>
            </div>

        </div>
<!--        <div class="slider_element mySlides_video fade video_sl">-->
<!--            <div class="date_gallery">-->
<!--                <p class="date-gallery-text">01 <span class="month">/ 17</span></p>-->
<!--            </div>-->
<!--            <div class="wrap related-media-wrapper flex">-->
<!---->
<!--                <div class="media-block wow fadeIn video_gallery_block" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="img/video2.png">-->
<!--                        <img src="img/video2.png">-->
<!--                    </a>-->
<!--                </div>-->
<!--                <div class="media-block  wow fadeIn video_gallery_block" style="visibility: visible; animation-name: fadeIn;">-->
<!--                    <a class="fancyboxAuto" rel="related-media" href="img/video1.png">-->
<!--                        <img src="img/video1.png">-->
<!--                    </a>-->
<!--                </div>-->
<!---->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--        <div class="container container_next"><a class="next next_videos" onclick="plusSlidesVideo(1)">далее</a></div>-->
    </div>
</section>
<?php endif; ?>