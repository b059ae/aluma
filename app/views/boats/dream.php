<?php
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $boat->seo('description')
]);
$this->title = $boat->seo('title', $boat->getTitle());

$asset = \app\assets\AppAsset::register($this);
?>
<?=$this->render('//boats/dream/_banner', ['asset' => $asset, 'boat' => $boat]); // Первый слайд ?>
<div style="text-align:center; margin-bottom: -6px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<style>
    .grey_text_char {
        color: rgba(255, 255, 255,0.9);
    }
</style>
<?=$this->render('//boats/_characteristics', ['asset' => $asset, 'boat' => $boat]); // Характеристики ?>
<div style="text-align:center; margin-bottom: -8px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?php /*=$this->render('//boats/_description', ['asset' => $asset]); // Преимущества ?>
<div  style="text-align:center; margin-bottom: -6px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_models', ['asset' => $asset, 'models' => $models]); // Модификации ?>
<div  style="text-align:center; margin-bottom: -6px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_order', ['asset' => $asset, 'boat' => $boat, 'equipment' => $equipment]); // Ваш заказ */ ?>
<?=$this->render('//boats/_gallery', ['boat' => $boat]); // Галерея  ?>