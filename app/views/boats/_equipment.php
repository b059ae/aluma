<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 21:11
 */
/** @var $equipment \yii\easyii\modules\catalog\models\Category[] */

/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */

use yii\easyii\modules\catalog\api\CategoryObject;

?>
<?php
// Смена комплектация по клику на миниатюру
$script = <<< JS
    // Какой-то хак для аккордеона. TODO: проверить 
    var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        }
    }
    // Открыть доп. оборудование
    function openEquipment(){
        $(".info__boat,.info__boat_div").show();
    }
    // Закрыть доп. оборудование
    function closeEquipment(){
        $(".info__boat,.info__boat_div").hide();
    }
    
    // Кнопка Закрыть доп. оборудование
    $(".close_info_btn").click(function () {
        closeEquipment();
    });
    // Кнопка Добавить оборудование
    $(".addtoorder").click(function () {
        openEquipment();
    });
    // Кнопка Подробнее
    $(".element__more_details").click(function () {
       var details = $(this).next(":hidden").html(); 
       $(".info__boat_block-right .info__boat-descript").hide().html(details).fadeIn();
       $("#display_info").show();
    });
    // Кнопка Назад
    $("#backtoinfo").click(function(){
		$("#display_info").hide();
	});
    // Кнопка количества. минус
    $(".count_minus").click(function () {
        var th = $(this);
        var count = parseInt(th.parents('.element_quantity').find('.count').text());
        count--;
        if(count < 1){
            count = 1;
        }
        th.parents('.element_quantity').find('.count').text(count);
    });
    // Кнопка количества. плюс
    $(".count_plus").click(function () {
        var th = $(this);
        var count = parseInt(th.parents('.element_quantity').find('.count').text());
        count++;
        if(count > 9){
            count = 9;
        }
        th.parents('.element_quantity').find('.count').text(count);
    });
    // Кнопку Добавить в расчет
    $(".add_to_count").click(function () {
        var th = $(this);
        var id = parseInt(th.attr('data-id'));
        var title = th.attr('data-title');
        var price = parseInt(th.attr('data-price'));
        var count = parseInt(th.parents('.boat__element').find('.count').text());
        
        shoppingCart.addItemToCart(id,title,price,count);
        closeEquipment();
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

<section class="info__boat" id="info_boat">
    <div class="container info__boat-container">
        <a
                href="#order"
                class="close_info_btn"
                rel="m_PageScroll2id"
                data-ps2id-offset="100"
        ></a>
        <div class="acc_container">

            <?php foreach ($equipment as $thread): ?>
                <div class="info__title accordion"> <?= $thread->title ?></div>
                <div class="info__boat_block panel">
                    <?php if (count($thread->children) > 0): ?>
                        <?php foreach ($thread->children as $subcat): ?>
                            <div class="info__subtitle info__subtitle-bottom"><?= $subcat->title ?></div>
                            <?= $this->render('_equipment_items', [
                                'items' => (new CategoryObject($subcat))->getItems(['filters' => ['suitablefor' => $boat->slug]]),
                            ]); ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <?= $this->render('_equipment_items', [
                            'items' => (new CategoryObject($thread))->getItems(['filters' => ['suitablefor' => $boat->slug]]),
                        ]); ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php // Описание товара ?>
        <div class="info__boat_block info__boat_block-right">
            <div class="info__boat-descript"></div>
        </div>
        <div class="info__boat_block info__boat_block-right" id="display_info">
            <div class="close_info_btn"></div>
            <div class = "info__boat-descript"></div>
            <button id="backtoinfo">Назад</button>
        </div>
    </div>
</section>
