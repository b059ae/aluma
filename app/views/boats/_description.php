<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 19:06
 */
/** @var $asset yii\web\AssetBundle */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
?>
<section class = "advantages">
    <div class="container advantages__container">
        <h2 class="advantages__title wow fadeInLeft">преимущества</h2>
        <?php /* <div class="logo_right"><img src="<?= $asset->baseUrl ?>/img/logo_right.png" alt=""></div>*/ ?>
        <img src="<?= $asset->baseUrl ?>/img/bg_boat-<?= $boat->slug ?>.png" alt="" class = "boat_scheme wow fadeInUp">
        <div class="advantages_description wow fadeInUp">
            <?= $boat->include ?>
        </div>
    </div>
</section>
