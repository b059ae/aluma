<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 19:06
 */
/** @var $asset yii\web\AssetBundle */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
/** @var $models \yii\easyii\modules\entity\api\ItemObject[] */
$countModels = count($models);
?>
    <section class="modifications">
        <div class="container modifications__container">
            <h2 class="modifications__title wow fadeInLeft">модификации</h2>

            <div class="slider_complects">
                <?php if ($countModels > 4): ?>
                    <button class="button-to-left">назад</button>
                <?php endif; ?>

                <?php foreach (array_slice($models, 0, 4) as $i => $model): ?>
                    <div class="slide_complect_block" id="model-slide-<?= $i ?>">
                        <span class="num_slide"><?= $i + 1 ?></span>
                        <p class="num_slide-text" data-id="<?= $i ?>"><?= $model->getTitle() ?></p>
                        <div class="boat_block <?=$boat->slug?>-<?= $i+1 ?>" data-id="<?= $i ?>"></div>
                        <div class="text-center">
                            <div class="price_boat"
                                 data-id="<?= $i ?>"><?= \Yii::$app->formatter->asDecimal($model->price, 0) ?> <i
                                        class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                        </div>
                        <?php /*
                        <a
                                href="#order"
                                onClick="shoppingCart.chooseModel('<?= $model->getTitle() ?>', <?= $model->price ?>)"
                                class="order_boat"
                                rel="m_PageScroll2id"
                        >заказать</a>*/ ?>
                    </div>
                <?php endforeach; ?>

                <?php if ($countModels > 4): ?>
                    <button class="button-to-right">Далее</button>
                <?php endif; ?>
            </div>
            <?php if ($countModels > 4): ?>
                <div class="slider_complects" style="display: none;">
                    <button class="button-to-left">назад</button>

                    <?php $offset = 4; ?>
                    <?php foreach (array_slice($models, $offset, 4) as $i => $model): ?>
                        <div class="slide_complect_block" id="model-slide-<?= $i + $offset ?>">
                            <span class="num_slide"><?= $i + $offset + 1 ?></span>
                            <p class="num_slide-text" data-id="<?= $i + $offset ?>"><?= $model->getTitle() ?></p>
                            <div class="boat_block <?=$boat->slug?>-<?= $i+$offset+1 ?>" data-id="<?= $i + $offset ?>"></div>
                            <div class="text-center">
                                <div class="price_boat"
                                     data-id="<?= $i + $offset ?>"><?= \Yii::$app->formatter->asDecimal($model->price, 0) ?>
                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                            </div>
                            <?php /*
                            <a
                                    href="#order"
                                    onClick="shoppingCart.chooseModel('<?= $model->getTitle() ?>', <?= $model->price ?>)"
                                    class="order_boat"
                                    rel="m_PageScroll2id"
                            >заказать</a> */?>
                        </div>
                    <?php endforeach; ?>

                    <button class="button-to-right">Далее</button>
                </div>
            <?php endif; ?>
        </div>
        <div style="clear: both"></div>
        <div id="models">
            <?php foreach ($models as $i => $model): ?>
                <div id="model-<?= $i ?>" class="models modif_descr container"
                     style="display: <?= $i == 0 ? 'block' : 'none' ?>;">

                    <div class="modifications__block modifications__block-1">
                        <div class="complect_number"><?= $model->getTitle() ?></div>
                        <?= $model->description ?>
                        <a
                                href="#order"
                                onClick="shoppingCart.chooseModel('<?= $model->getTitle() ?>', <?= $model->price ?>)"
                                class="add_to_order orng_btn"
                                rel="m_PageScroll2id"
                        >выбрать этот катер</a>
                    </div>
                    <div class="boat__complectation">
                        <img src="<?= $asset->baseUrl ?>/img/models/<?=$boat->slug?>/<?= $i+1 ?>.png?20180304" alt="">
                    </div>
                    <div class="modifications__block modifications__block-2">
                        <?= $model->equipment ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php
$script = <<< JS
    // Смена комплектация по клику на миниатюру
    function modelShow(id) {
        // Анимация
        $(".models").hide();
        $("#model-"+id).fadeIn(1500);
        
        $(".slide_complect_block").removeClass('active_complectation');
        $("#model-slide-"+id).addClass('active_complectation');
    }
    $('.boat_block,.price_boat,.num_slide-text').click(function() {
        var id = $(this).attr('data-id');
        modelShow(id);
        $.mPageScroll2id("scrollTo","#models",{
            offset: 200
        });
    });
    // modelShow(0);
    
    // Слайдер
    var sliderIndex = 1;
    // showSl(sliderIndex);
    
    $('.button-to-right').click(function() {
        showSl(sliderIndex += 1);
    });
    $('.button-to-left').click(function() {
        showSl(sliderIndex -= 1);
    });
    
    function showSl(n) {
      var i;
      var x = document.getElementsByClassName("slider_complects");
      if (n > x.length) {sliderIndex = 1}    
      if (n < 1) {sliderIndex = x.length}
      var sl = sliderIndex - 1;
      
      // Анимация
      $(".slider_complects:visible").hide();
      $($(".slider_complects")[sl]).fadeIn(1000);
    }
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>