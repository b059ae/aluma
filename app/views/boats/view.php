<?php
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
/** @var $equipment \yii\easyii\modules\catalog\api\CategoryObject[] */
/** @var $models \yii\easyii\modules\entity\api\ItemObject[] */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $boat->seo('description')
]);
$this->title = $boat->seo('title', $boat->getTitle());

$asset = \app\assets\AppAsset::register($this);
?>
<?=$this->render('//boats/_banner', ['boat' => $boat]); // Первый слайд ?>
<div style="text-align:center; margin-bottom: -6px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_characteristics', ['asset' => $asset, 'boat' => $boat]); // Характеристики ?>
<div style="text-align:center; margin-bottom: -8px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_description', ['asset' => $asset, 'boat' => $boat]); // Преимущества ?>
<div  style="text-align:center; margin-bottom: -6px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_models', ['asset' => $asset, 'boat' => $boat, 'models' => $models]); // Модификации ?>
<div  style="text-align:center; margin-bottom: -6px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_order', ['asset' => $asset, 'boat' => $boat, 'equipment' => $equipment]); // Ваш заказ ?>
<div style="text-align:center; margin-bottom: -8px;"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_gallery', ['boat' => $boat]); // Галерея ?>