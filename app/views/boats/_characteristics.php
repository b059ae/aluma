<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 19:05
 */
/** @var $asset yii\web\AssetBundle */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
?>
<section class="characteristics <?= $boat->slug ?>">
    <div class="container container-char">
        <h2 class="characteristics__title wow fadeInLeft">характеристики</h2>
        <div class="wrapper_text_char wow fadeInUp">
            <?= $boat->characteristics ?>
        </div>
    </div>
</section>