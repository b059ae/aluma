<?php
/**
 * User: Alexander Popov
 * Email: b059ae@gmail.com
 * Date: 09/12/2017 19:39
 */

/** @var $items \yii\easyii\modules\catalog\api\ItemObject[] */
?>
<?php foreach ($items as $item): ?>
    <div class="boat__element">
        <?php if (!empty($item->model->image_file)) : ?>
        <img src="<?= $item->thumb(58, 58) ?>" alt="">
        <?php endif; ?>
        <div class="table__element">
            <p class="element__name"><?= $item->getTitle() ?></p>
            <?php if (strlen($item->getDescription()) > 0): ?>
                <a
                        href="#info_boat"
                        class="element__more_details"
                        rel="m_PageScroll2id"
                        data-ps2id-offset="50"
                >подробнее</a>
                <div style="display: none;"><?= $item->getDescription() ?></div>
            <?php endif; ?>
        </div>
        <div class="table__element element_quantity">
            <div class="count_minus"><i class="fa fa-minus" aria-hidden="true"></i></div>
            <span class="count">1</span>
            <div class="count_plus"><i class="fa fa-plus" aria-hidden="true"></i></div>
        </div>
        <div class="table__element element_price"> <?= \Yii::$app->formatter->asDecimal($item->price, 0) ?> <i
                    class="fa fa-rub" aria-hidden="true"></i>
        </div>
        <div class="table__element">
            <a
                    href="#order"
                    class="add_to_count"
                    data-id="<?= $item->id ?>"
                    data-title="<?= $item->getTitle() ?>"
                    data-price="<?= $item->price ?>"
                    rel="m_PageScroll2id"
                    data-ps2id-offset="100"
            ><i class="fa fa-shopping-basket" aria-hidden="true"></i><span>добавить в расчёт</span></a>
        </div>
    </div>
<?php endforeach;  ?>
