<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 19:05
 */
/** @var $asset yii\web\AssetBundle */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
?>
<section class = "aluma__homepage" style="background-image: url('<?= $asset->baseUrl ?>/img/dream.jpg')">
    <div class="container">
        <h2 class="title-aluma__homepage dream-6500">
            Серийное производство <br>алюминиевых катеров
        </h2>
        <?=\app\widgets\bannerForm\BannerForm::widget(['boat' => $boat->getTitle()]); ?>
    </div>
</section>
