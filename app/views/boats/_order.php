<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.12.2017
 * Time: 19:06
 */
/** @var $asset yii\web\AssetBundle */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
/** @var $equipment \yii\easyii\modules\catalog\api\CategoryObject[] */
?>
<section id="order" class="advantages your_order">
    <div class="container advantages__container">
        <h2 class="advantages__title wow fadeInLeft">ваш заказ</h2>
        <div class="container container_order wow fadeInUp">
            <div class="order_table_container">
                <div class="table_your_order">
                    <div class="container_table">
                        <div class="table_order_col table_order_data-left">
                            <div class="table_order_title">Наименование</div>
                        </div>
                        <div class="table_order_col table_order_data-right table_order_data-right-1">
                            <div class="table_order_title">Количество</div>
                        </div>
                        <div class="table_order_col table_order_data-right table_order_data-right-2">
                            <div class="table_order_title">Цена</div>
                        </div>

                    </div>

                </div>
                <div class="buttons_your_order">
                    <a href="#models" class="addequipment" rel="m_PageScroll2id" data-ps2id-offset="450">выбрать
                        катер</a>

                    <a class="price-boat"><span id="boat-price">480 000</span> <i class="fa fa-rub" aria-hidden="true"></i></a>
                </div>
            </div>

            <div class="order_options visible-mobile wow fadeInUp">
                <?= \app\widgets\callback\CallbackButton::widget([
                    'title' => 'оформить заказ',
                    'buttonCssClass'=>'get_order_consult',
                    'buttonTitle'=>'оформить заказ',
                    'boat'=>$boat->getTitle(),
                ]); ?>

                <a class="finish_order addtoorder" rel="m_PageScroll2id" href="#info_boat">добавить
                    оборудование</a>
            </div>

            <?=\app\widgets\gift\GiftForm::widget(['boat'=>$boat->getTitle()]);?>

            <div class="order_options hidden-mobile wow fadeInUp">
                <?= \app\widgets\callback\CallbackButton::widget([
                        'title' => 'оформить заказ',
                        'buttonCssClass'=>'get_order_consult',
                        'buttonTitle'=>'оформить заказ',
                        'boat'=>$boat->getTitle(),
                ]); ?>

                <a class="finish_order addtoorder" rel="m_PageScroll2id" href="#info_boat">добавить
                    оборудование</a>
            </div>
        </div>
    </div>
</section>
<div  style="text-align:center; margin-bottom: -6px;" class = "info__boat_div"><img src="<?= $asset->baseUrl ?>/img/bottom.jpg" alt=""></div>
<?=$this->render('//boats/_equipment', ['boat' => $boat, 'equipment' => $equipment]); // Доп. оборудование ?>