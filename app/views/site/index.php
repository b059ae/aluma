<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */
/** @var $boat \yii\easyii\modules\catalog\api\ItemObject */
/** @var $materials \yii\easyii\modules\entity\api\Entity[] */
/** @var $news \yii\easyii\modules\article\models\Item[] */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->title = $page->seo('title');

$asset = \app\assets\AppAsset::register($this);
?>

<?= $this->render('//common/_banner2', ['asset' => $asset]); ?>
<?php /*echo $this->render('//common/_mini_nav', []); */?>
<?php /*echo $this->render('index/_description', ['boats' => [$boat]]); */?>
<?php /*echo $this->render('index/equipment/_equipment', ['boat' => $boat]); */?>
<?php /*echo $this->render('//boats/_characteristics', ['asset' => $asset, 'boat' => $boat]); */?>
<?php /*echo $this->render('//boats/_gallery', ['boat' => $boat]); */?>
<?php /*echo $this->render('index/_materials', ['materials' => $materials]); */?>
<?php /*echo $this->render('index/_about', ['page' => $page]); */?>
<?php /*echo $this->render('//news/_index', ['news' => $news]); */?>
