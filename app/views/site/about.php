<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->title = $page->seo('title', $page->getTitle());
?>

<?=$this->render('index/_about', ['page' => $page]); ?>
<?=$this->render('index/_materials', ['materials' => $materials]); ?>
<?=$this->render('//news/_index', ['news' => $news]); ?>