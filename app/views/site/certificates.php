<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->title = $page->seo('title', $page->getTitle());
?>
<section class="latest-news pad-100">
    <div class="container modifications__container">
        <h1 class="modifications__title wow fadeInLeft"><?=$this->title ?></h1>
        <?=$page->getText() ?>
    </div>
</section>