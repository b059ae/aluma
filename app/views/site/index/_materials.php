<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 27.05.2017
 * Time: 16:54
 */
use yii\easyii\helpers\Image;
use yii\web\AssetBundle;

/** @var $materials \yii\easyii\modules\entity\api\Entity[] */
?>
<section class = "advantages">
    <div class="container advantages__container" style="background-color: #fff;">
        <h2 class="advantages__title wow fadeIn">Материалы и технологии</h2>
        <div class="feature-posts-wrap flex pad-bottom-40 wow fadeIn">
        <?php foreach ($materials as $material): ?>
            <div class="feature-cards flex-col-4">
                <div class="feature-cards-single flex">
                    <div class="feature-cards-image flex-col-12"
                         style="background-image: url('<?= Image::thumb($material->photo, 390, 100) ?>')">
                    </div>
                    <div class="feature-cards-info flex-col-12">
                        <h3><?= $material->title ?></h3>
                        <div class="post-snippet"><?= $material->description ?></div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    </div>
</section>

