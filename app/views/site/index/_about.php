<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 27.05.2017
 * Time: 16:54
 */

/** @var $page \yii\easyii\modules\page\api\PageObject */
?>
<?php if (!empty($page->getText())): ?>
    <section id="o-kompanii" class="grey center pad-100">
        <div class="container modifications__container">
            <h1 class="modifications__title wow fadeInLeft">О компании</h1>
            <div class="legacyContainer">
                <div class="historySectionContainer video-container">
                    <div class="container legacy historyContent wow fadeInUp">
                        <?= $page->getText() ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
