<?php
namespace app\models\interfaces;
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 11.08.17
 * Time: 14:21
 */

interface CallerInterface
{
    /**
     * Звонок абоненту
     * @param $phoneNumber string
     */
    public function call($phoneNumber);
}