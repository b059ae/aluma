<?php

namespace app\models;

use app\components\Beeline;
use app\validators\NameMatchValidator;
use app\validators\PhoneFilterValidator;
use app\validators\PhoneMatchValidator;
use yii\easyii\modules\feedback\api\Feedback;

abstract class AbstractFeedbackForm extends \yii\base\Model
{
    public $name;
    public $phone;
    public $email;
    public $text;
    public $boat;

    public function formName()
    {
        return 'Feedback';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name'], NameMatchValidator::class],
            [['email'], 'email'],
            [['text', 'boat'], 'string'],
            ['phone', PhoneFilterValidator::class],
            ['phone', PhoneMatchValidator::class],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $text = $this->text;

        if (strlen($this->boat) > 0) { // Заявка со страницы лодки
            // Название лодки
            $text .= PHP_EOL . $this->boat;
            // Корзина
            $session = \Yii::$app->session;
            // Комплектация
            $boatModel = json_decode($session->get('model', ''));
            if (strlen($boatModel) > 0) {
                $text .= PHP_EOL . $boatModel;
            }
            // Дополнительное оборудование
            $shoppingCart = json_decode($session->get('shoppingCart', '[]'));
            if (count($shoppingCart) > 0) {
                $text .= PHP_EOL . "Дополнительное оборудование:" . PHP_EOL;
                foreach ($shoppingCart as $item) {
                    $text .= $item->name . " " . $item->count . " шт." . PHP_EOL;
                }
            }
        }

        // Создание заявки в AmoCRM
        // TODO: Создание заявки в AmoCRM

        // Сохранение заявки с сайта
        try {
            $model = new Feedback();
            $model->api_save([
                'name' => $this->name,
                'phone' => $this->phone,
                'email' => $this->email,
                'text' => $text,
            ]);
        } catch (\Exception $e) {
        }

        // Автоматический звонок
        try {
            /** @var Beeline $caller */
            $caller = \Yii::$app->beeline;
            $caller->call($this->phone);
        } catch (\Exception $e) {
        }

        return true;
    }
}
