<?php

namespace app\controllers;

use Yii;
use yii\easyii\modules\subscribe\models\Subscriber;
use yii\web\Controller;

class SubscribeController extends Controller
{
    /**
     * Подписка на рассылку
     * @return string
     */
    public function actionSend()
    {
        $model = new Subscriber;
        $request = Yii::$app->request;

        if ($model->load($request->post())) {
            $model->save();
        }
        return ;
    }
}