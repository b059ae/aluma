<?php

namespace app\controllers;

use yii\easyii\models\Tag;
use yii\easyii\modules\article\api\Article;

class NewsController extends \yii\web\Controller
{
    /*public function actionIndex()
    {
        return $this->render('index');
    }*/

    /**
     * Список постов
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $news = Article::cat('news');
        if(!$news){
            throw new \yii\web\NotFoundHttpException('News category not found.');
        }
        //$tags = Tag::find()->orderBy('frequency DESC')->limit(10)->asArray()->all();

        return $this->render('index', [
            'news' => $news->getItems([
                'pagination' => ['pageSize' => 10],
            ]),
            'cat' => $news,
//            'tags'=>$tags,
        ]);
    }

    /**
     * Полный текст поста
     * @param string $slug
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($slug)
    {
        $cat = Article::cat('news');
        if(!$cat){
            throw new \yii\web\NotFoundHttpException('Article category not found.');
        }
        $article = Article::get($slug);
        if(!$article){
            throw new \yii\web\NotFoundHttpException('Article not found.');
        }

        return $this->render('view', [
            'article' => $article,
        ]);
    }

}
