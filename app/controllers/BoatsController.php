<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 02.06.2017
 * Time: 23:32
 */

namespace app\controllers;

use app\api\EntitySlugItems;
use app\models\CategoryModel;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\entity\api\Entity;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Session;

class BoatsController extends Controller
{
    /**
     * Просмотр информации о лодке Fish
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $boat = Catalog::get($slug);
        if (!$boat) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('view', [
            'boat' => $boat, // Описание лодки
            'equipment' => \app\helpers\Catalog::tree('equipment'), // Доп. оборудование
            'models' => Entity::cat('model-'.$slug)->getItems(), // Комплектации
            /*'features' => Entity::cat('feature-'.$slug)->getItems(),// Функциональность
            'reliability' => Entity::cat('reliability-'.$slug)->getItems(),// Надежность
            'driving' => Entity::cat('driving-'.$slug)->getItems(),// Надежность
            'photo' => EntitySlugItems::cat('photo')->getItems(),// Фото
            */
        ]);
    }

    /**
     * Просмотр информации о лодке Dream
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDream($slug)
    {
        $boat = Catalog::get($slug);
        if (!$boat) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('dream', [
            'boat' => $boat, // Описание лодки
        ]);
    }

    public function actionCart(){
        /** @var Session $session */
        $session = \Yii::$app->session;
        $session->set('shoppingCart', \Yii::$app->request->post('shoppingCart'));
        $session->set('model', \Yii::$app->request->post('model'));
    }
}