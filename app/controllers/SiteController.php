<?php

namespace app\controllers;

use app\api\EntitySlugItems;
use app\widgets\bannerForm\actions\ConsultationAction;
use app\widgets\gift\actions\GiftAction;
use yii\easyii\modules\article\api\Article;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\entity\api\Entity;

use yii\easyii\modules\page\api\Page;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'page' => $this->getPage('index'), // Страница
            'boat' => Catalog::get('fish-47'),// Описание лодки
            'materials' => Entity::cat('materials')->getItems(),// Материалы
            'news' => Article::cat('news')->getItems([
                'pagination' => ['pageSize' => 3],
            ]), // Новости
            /*
              'characteristics' => Entity::cat('characteristics-fish-47')->getItems(),// Материалы
              'video' => EntitySlugItems::cat('video')->getItems(),// Видео
            'features' => Entity::cat('feature-fish-47')->getItems(),// Функциональность
            'photo' => EntitySlugItems::cat('photo')->getItems(),// Фото*/
        ]);
    }

    /**
     * О компании
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAbout(){
        return $this->render('about', [
            'page' => $this->getPage('about'), // Страница
            'materials' => Entity::cat('materials')->getItems(),// Материалы
            'news' => Article::cat('news')->getItems([
                'pagination' => ['pageSize' => 3],
            ]), // Новости
        ]);
    }

    /**
     * Сертификаты
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCertificates(){
        return $this->render('certificates', [
            'page' => $this->getPage('certificates'), // Страница
        ]);
    }

    /**
     * Видеообзоры
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionVideos(){
        return $this->render('videos', [
            'page' => $this->getPage('videos'), // Страница
        ]);
    }

    /**
     * @param string $slug
     * @return \yii\easyii\modules\page\api\PageObject
     * @throws NotFoundHttpException
     */
    protected function getPage($slug){
        $page = Page::get($slug);
        if (!$page) {
            throw new NotFoundHttpException('Item not found.');
        }
        return $page;
    }
}