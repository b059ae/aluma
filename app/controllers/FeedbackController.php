<?php

namespace app\controllers;

use app\widgets\bannerForm\actions\BannerCardAction;
use app\widgets\callback\actions\CallbackAction;
//use app\widgets\consultationForm\actions\ConsultationAction;
use app\widgets\gift\actions\GiftAction;
use yii\web\Controller;

class FeedbackController extends Controller
{
    public function actions()
    {
        return [
            // Форма на первом слайде
            'banner' => BannerCardAction::class,
            // Форма на втором слайде
            'gift' => GiftAction::class,
            // Форма на третьем слайде
            //'consultation' => ConsultationAction::class,
            // Форма обратного звонка в футере
            'callback' => CallbackAction::class,
        ];
    }
}