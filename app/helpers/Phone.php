<?php
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 10.06.2017
 * Time: 10:20
 */

namespace app\helpers;


class Phone
{
    /**
     * Форматирование телефона для использования в ссылке
     * @param string $phone
     * @return string
     */
    public static function link($phone)
    {
        return '+7'.substr(preg_replace('/[^0-9]+/', '', $phone), 1);
    }
}