<?php

namespace app\helpers;
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 02.06.17
 * Time: 15:51
 */
Class Boat
{

    const VISIBLE_INCLUDE_ITEMS = 5;

    /**
     * Форматирование вывода для элементов, которые включены в стоимость
     * @param string $text
     * @return string
     */
    public static function formatInclude($text)
    {
        $result = '';

        $paragraphs = array_filter(preg_split('#<p([^>])*>#', str_replace('</p>', '', $text)));
        foreach ($paragraphs as $i => $row) {
            $class = $i++ > self::VISIBLE_INCLUDE_ITEMS
                ? 'hidden'
                : '';
            $result .= '<div class="boat-builder-box-info-sections ' . $class . '">';
            $result .= '<p class="boat-section-title">' . $row . '</p>';
            $result .= '</div>';
            /*
             * Убран формат Заголовок-описание. Осталься только заголовок

                if ($i%2){
                    $result.='<div class="boat-builder-box-info-sections">';
                    $result.='<p class="boat-section-title">'.$row.'</p>';
                }else{
                    $result.='<p class="boat-section-description">'.$row.'</p>';
                    $result.='</div>';
            }*/
        }

        return $result;
    }
}