<?php

namespace app\helpers;
/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 09.12.17
 * Time: 15:51
 */
Class Catalog
{
    /**
     * Returns array of categories in tree structure.
     * @param string $slug
     * @return array
     */
    public static function tree($slug)
    {
        $tree = \yii\easyii\modules\catalog\api\Catalog::tree();
        foreach ($tree as $thread){
            if ($thread->slug == $slug){
                return $thread->children;
            }
        }

        return [];
    }
}