$(document).ready(function() {
    /* SlickNav */
    $('#mobile-nav').slicknav({
        label: '',
        prependTo: '',
        appendTo: '.build-mobile-wrapper',
        duplicate: false
    });
   /* $('#main-navigation a[href^="/#"]').click(function(e){
        // Главная страница
        if (window.location.pathname != '/') {
            return true;
        }
        e.preventDefault();
        var scrollTo = $(this).attr('href').slice(1);
        if ($(scrollTo).length != 0) {
            $("html, body").animate({
                scrollTop: $(scrollTo).offset().top - $(".header").height()
            }, 500);
            return false;
        }
    });*/
    $("a[rel='m_PageScroll2id']").mPageScroll2id();
    /*Скрол при переходе с другой страницы*/
    // *only* if we have anchor on the url
    /*if(window.location.hash) {
        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top - $(".header").height()
        }, 500);
    }*/

    // Show / hide nav bar / sneaky nav
    var position = $(window).scrollTop(); // should start at 0
    scroll_lock = false;
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        // be sure nav isn't open
        if( !scroll_lock && $(".header-bottom").find(".active").length == 0 ) {
            if (scroll > position && scroll > 150) {
                $('div.sneak').addClass('closed');
                scroll_lock = true;
            } else {
                $('div.sneak').removeClass('closed');
                scroll_lock = true;
            }

            setTimeout("scroll_lock = false;", 100);
            position = scroll;
        }

    });


// used for popups
    $(".fancybox").fancybox({
        type: 'inline',
        maxWidth: 957,
        maxHeight: 1000,
        fitToView: true,
        width: '100%',
        height: '100%',
        closeClick: false,
        scrolling: 'auto',
        padding: 0,
        helpers: {
            overlay: {
                locked: true
            }
        },
        /*beforeShow: function() {
            this.width = $('.fancybox-iframe').contents().find('html').width();
            this.height = $('.fancybox-iframe').contents().find('html').height();
        },
        afterClose: function() {
            update_location_div('false');
        }*/
    });

    $('.fancybox-video').fancybox({
        maxWidth: 960,
        maxHeight: 550,
        width: '100%',
        height: '100%',
        padding: 0,
        type: 'iframe',
        helpers: {
            overlay: {
                locked: true
            }
        }
    });

// images, all that is needed for groups is just to use a rel on the link/image that has the fancybox assigned to it.
    $(".fancyboxAuto").fancybox({
        autoSize: true,
        fitToView: true,
        closeClick: false,
        scrolling: 'auto',
        margin: 0,
        padding: 0,
        helpers: {
            overlay: {
                locked: true
            }
        }
    });

    // Animate js (wow.js)
    wow = new WOW( {
            boxClass:     'wow',
            animateClass: 'animated',
            offset:       10
        }
    );

    wow.init();

    // Add animateCss
    $.fn.extend({
        animateCss: function (animationName, callback) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
                if (callback) {
                    callback();
                }
            });
            return this;
        }
    });

    // Parallax
    /*(function(){

        var parallax = document.querySelectorAll(".parallax"),
            speed = -0.5;

        window.onscroll = function(){
            [].slice.call(parallax).forEach(function(el,i){

                var windowYOffset = window.pageYOffset,
                    elBackgrounPos = "50% " + (windowYOffset * speed) + "px";

                el.style.backgroundPosition = elBackgrounPos;

            });
        };

    })();*/


    $('#mobile-nav > li').click(function (event) {
        $(this).children("ul").slideToggle();
        event.stopPropagation();
    });

    $('#dropbtn').click(function () {
        var myDropdown = $("#drop_catalog");
        if (myDropdown.classList.contains('show')) {
            myDropdown.classList.remove('show');
        }
    });

    $('.banner-left').click(function () {
        $('.banner-main').hide();
        $('.banner-fish').show(500);
    });
});
function dropdownCatalog() {
    document.getElementById("drop_catalog").classList.toggle("show");
}

// Заказ
// ***************************************************
// Shopping Cart functions

var shoppingCart = (function () {
    // Public property
    var obj = {};

    // Private methods and properties
    var cart = [];
    var model = '';
    var modelPrice = 0;

    function Item(id, name, price, count) {
        this.id = id
        this.name = name,
        this.price = price
        this.count = count
    }

    function saveCart() {
        var data = JSON.stringify(cart);
        localStorage.setItem("shoppingCart", data);
        var modelData = JSON.stringify(model);
        localStorage.setItem("model", modelData);
        var modelPriceData = JSON.stringify(modelPrice);
        localStorage.setItem("modelPrice", modelPriceData);
        $.post({
            url: "/boats/cart",
            data: {"shoppingCart": data, "model": modelData}
        });
        renderOrder();
    }

    function loadCart() {
        cart = JSON.parse(localStorage.getItem("shoppingCart"));
        if (cart === null) {
            cart = []
        }
        model = JSON.parse(localStorage.getItem("model"));
        if (model === null) {
            model = ''
        }
        modelPrice = JSON.parse(localStorage.getItem("modelPrice"));
        if (modelPrice === null) {
            modelPrice = 0
        }
        renderOrder();
    }

    function renderOrder() { // Рендеринг заказа
        var tname = $(".table_order_data-left").find(".table_order_title");
        var tcount = $(".table_order_data-right-1").find(".table_order_title");
        var tprice = $(".table_order_data-right-2").find(".table_order_title");
        var templateName = '<div class="table_order_data">{delete} {name}</div>';
        var templateCount = '<div class="table_order_data">{count}</div>';
        var templatePrice = '<div class="table_order_data">{price}</div>';
        var templateDelete = '<i class="fa fa-times" aria-hidden="true" onClick="shoppingCart.removeItemFromCart({id})"></i>';
        // Удалить заказ
        $(".table_order_data-left").find(".table_order_data").remove();
        $(".table_order_data-right-1").find(".table_order_data").remove();
        $(".table_order_data-right-2").find(".table_order_data").remove();

        // Оборудование
        for (var i in cart.reverse()) {
            tname.after(templateName.replace("{name}", cart[i].name).replace("{delete}",templateDelete.replace("{id}", cart[i].id)));
            tcount.after(templateCount.replace("{count}", cart[i].count));
            tprice.after(templatePrice.replace("{price}", cart[i].price.toLocaleString('ru-RU')));
        }

        // Комплектация
        if (model.length > 0) {
            tname.after(templateName.replace("{name}", model).replace("{delete}", ""));
            tcount.after(templateCount.replace("{count}", 1));
            tprice.after(templatePrice.replace("{price}", modelPrice.toLocaleString('ru-RU')));
        }

        $("#boat-price").text(obj.totalCart().toLocaleString('ru-RU'));
    }


    // Public methods
    obj.chooseModel = function (name, price) { // Выбор комплектации
        console.log('model price = '+price);
        console.log('model name = '+name);
        model = name;
        modelPrice = price;
        saveCart();
    };

    obj.addItemToCart = function (id, name, price, count) { // Добавление доп. оборудования
        console.log("addItemToCart -- ");
        for (var i in cart) {
            if (cart[i].id === id) {
                cart[i].count += count;
                saveCart();
                return;
            }
        }

        console.log("addItemToCart:", id, name, price, count);

        var item = new Item(id, name, price, count);
        cart.push(item);
        saveCart();
    };

    obj.setCountForItem = function (id, count) {
        for (var i in cart) {
            if (cart[i].id === id) {
                cart[i].count = count;
                break;
            }
        }
        saveCart();
    };


    obj.removeItemFromCart = function (id) { // Removes one item
        for (var i in cart) {
            if (cart[i].id === id) { // "3" === 3 false
                cart[i].count--; // cart[i].count --
                if (cart[i].count === 0) {
                    cart.splice(i, 1);
                }
                break;
            }
        }
        saveCart();
    };


    obj.removeItemFromCartAll = function (id) { // removes all item name
        for (var i in cart) {
            if (cart[i].id === id) {
                cart.splice(i, 1);
                break;
            }
        }
        saveCart();
    };


    obj.clearCart = function () {
        cart = [];
        saveCart();
    }


    obj.countCart = function () { // -> return total count
        var totalCount = 0;
        for (var i in cart) {
            totalCount += cart[i].count;
        }

        return totalCount;
    };

    obj.totalCart = function () { // -> return total cost
        var totalCost = modelPrice;
        for (var i in cart) {
            totalCost += cart[i].price * cart[i].count;
        }
        return totalCost;
    };

    obj.listCart = function () { // -> array of Items
        var cartCopy = [model];
        console.log("Listing cart");
        console.log(cart);
        for (var i in cart) {
            console.log(i);
            var item = cart[i];
            var itemCopy = {};
            for (var p in item) {
                itemCopy[p] = item[p];
            }
            itemCopy.total = (item.price * item.count).toFixed(2);
            cartCopy.push(itemCopy);
        }
        return cartCopy;
    };

    loadCart();

    // ----------------------------
    return obj;
})();




